---
layout: default
permalink: /author/
---
<section id = 'wrapper'>
  <section class = 'cool'>
    <div class = 'overlay flex-in'>
      <div class = 'child tripple'>
        <div id = 'author'>
          <img src = '/assets/{{site.gravatar}}' alt = 'blog author'>
        </div>
        <div>
          <h1>I'm a software developer</h1>
          <p class = 'mute center'>{{ site.designer.desc }}</p>
        </div>
      </div>
    </div>
  </section>
  <section class = 'transparent'>
    <h2>How do I keep up?</h2>
    <p class = 'pale center'>I'm a self taught  developer, and I'm proficient in following fields</p>
    <div class = 'skills wrap'>
      {% for skill in site.data.skills %}
        <div class = 'skill skill_{{ forloop.index }}' style = 'border-left: 5px solid {% cycle '#04a763', '#24292e', '#ff4447' %}'>{{skill }}</div>
      {% endfor %}
    </div>
    <div class = 'project__container'>
      <h2 id = 'projects'>Some of My Projects</h2>
      <div class = 'flex-in projects'>
      {% for project in site.data.projects %}
      {% if forloop.index > 3 %}
          {% assign nupe = 'nupe' %}
      {% endif %}
        <div class = 'child third project {{ nupe }}'>
            <a  href = '{{ project.link }}' target = '_blank'  rel="noopener"> 
              <img src = '/assets/projects/{{project.thumb}}.png' class = 'project__thumb' alt = 'logo'>
              {{ project.name }}
            </a>
        </div>
      {% endfor %}
      </div>
    </div>
  </section>
</section>
