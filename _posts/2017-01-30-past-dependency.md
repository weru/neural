---
tags: 
  - perspective
image: hand
---

Dependency in a society should only be reserved for those who due to age or disability cannot possibly fed themselves.
Time and time again, people have overcome difficulties to rise above their circumstances. Unfortunately, today's society has been
crippled with complacency and entitlement. Everyone wants to follow the crowd, to go down the easy familiar path.

Probably that is where many of us go wrong. But how can we help but be what we are, if it is the way we have been raised to know.
Our parents will say to us, 'my son, my daughter, go to school and study really hard and hopefully you will get a good job'. Our 
society cheers us on to be heroes of academia, to secure that 'big' classy job. As a result we are conditioned to hope for an 
opportunity while little emphasis is placed in exploring new unfamiliar ideas. The society is emaciated of creators of
opportunities - many individuals would rather be beneficiaries of opportunities. How many times are untried ideas met with skepticism?
How many people would rather point out that something is 'probably' not a cool idea? 

It is easy to be hopeful than it is easy to take risks. That's why Jim Carrey once said, 'hope is a beggar but faith leaps through fire'.
Not that hope isn't necessary, but hope alone has never been enough, it needs to be paired with practiced faith if it is ever to 
materialize.

If we could look past our entitlement - 'I have a college degree, I should get a job'. Or 'I need a PHD to get a raise.' Instead,
we should be like, what can I do with the knowledge I have amassed so far to better the world I find myself in and simultaneously
earn from it? How could I be useful?

We have sharpened our axes enough; we're as privileged as we are talented. Preparation alone isn't enough unless it is tested. 
Better yet, many at times it is better to dive enough and learn while we're at it. Yeah, it is crucial that we get all the excuses
we use as buffers to push ahead, to be creators.

This way, we will break away from all kinds of dependency. We shall no longer be dependent of our governments to create jobs
for us. After all, governments don't really create jobs but an enabling environment for entrepreneurial activity. We shall no 
longer be dependent on our relatives to secure us positions on their work places nor shall we be dependent on our papers to get us
the jobs we need. Instead, we will apply our knowledge and skills to bring about some meaningful change for others and ourselves.
