---
tags:
  - perspective
image: thrive
---
Here is a man that knows the pain of suffering and the ills of inaction. He understands that to trump on the former, the latter has to be despised in all it forms. Experience has something to do with the person he is. Most likely he has let himself down before - dozens of times perhaps. That is why being hard on himself is not a privelege but an absolute necessity.

He will never allow himself to be a losing man - because he has been that man. He hates the price that comes with losing. All that there is for him is soaring. Not that he doesn't fall from time to time. Yeah, he fails more than the [losing man](/a-losing-man/) can in a dozen lifetimes. That's because he risks more, and therefore, fails as many times as it takes him to win. Through momentary fleeting failures, he molds himself into an eagle.

With unconscious awareness, he responds like an eagle. An eagle will take advantage of a storm to soar to heights that it wouldn't in fair weather. In fact, eagles have been observed to head into the storm in a strategic manuever that takes advantage of the storm. The finer details of how this bird achieves its goals are very intriguing and could take an entire post to describe. Nature is deep.

Like an eagle, this man embraces catastrophy, not because he particularly enjoys discomfort but because he understands that bad times provide tremenduous opportunities. Whenever tragedy strikes, he tunes his dials to spot an opportunity or a set of them. He likes to focus and therefore, always narrows down to the best of them, or the ones that can be knitted into one path.

## Tells of a thriving man

### He values details
  
Everything is made of something - finer pieces arranged in a specific manner. Diamonds and graphite are made of the same stuff (carbon atoms). Yet they are very different in form, shape, appeal and value. The differences between the two substances come from how the carbon atoms are linked to each other - the how. 

For a man to thrive, he ought to go beyond 'the what' and 'the why' - he needs to be curiuous. This trait has helped humanity get ahead throughout history. How can I/we be do to improve my/our circumstances? Then narrow down the question to the individual. Then he gets down to working out the details, with a persistent resolve to rest only when the question is actively settled.

### He keeps good company

We're as good as what and who we learn from. Our environs shape the people we become. If we stay positive and keep cultivate resourceful friendships, we're bound at some point to pick good traits. To thrive, one needs to build a rich network of friends and tools - not large, but rich. The depth and quality of the company a man keeps may expand his vision, propels him into success, trumps on his talents, or gradually piss on his dreams. A wise man knows how to curve out the best he can.

Wisdom helps a man seek out tools connect with resourceful individuals from other cultures, countries or from the past. A good book can be worth a dozen teachers. Reading is a formidable tool if one knows where to look - and make the necessary investment. Reading is another form of keeping good company.

### He creates the mood necessary for *'the hustle'*

It's hard to get anything done in a world where there are so many things crying for our attention. When our own bodies and mind don't feel like doing what needs to be done. No, it's not easy for a man who is waiting for the perfect moment to get off his ass and change his life. How could he? The circumstances aint right, you know. This is where a thriving man outshines the masses. He understands the value of discipline. He decides that he is gonna get his hands dirty even when the circumstances are anything but. His genius is in his belief that the right circumstances will eventually catch up with him. By doing what needs to be done, he will learn to get into the mood. Therefore, he approaches everything in life with a proactive attitude.

To sum up, consider [Jim Cantrell's](https://www.quora.com/Elon-Musk/How-did-Elon-Musk-learn-enough-about-rockets-to-run-SpaceX) take on [Elon Musk](https://en.m.wikipedia.org/wiki/Elon_Musk) - the man who now spearheads space travel, electric cars, and solar power utilization:

***So I am going to suggest that he is successful not because his visions are grand, not because he is extraordinarily smart and not because he works incredibly hard. All of those things are true. The one major important distinction that sets him apart is his inability to consider failure. It simply is not even in his thought process. He cannot conceive of failure and that is truly remarkable. It doesn't matter if its going up against the banking system (Paypal), going up against the entire aerospace industry (SpaceX) or going up against the US auto industry (Tesla). He can't imagine NOT succeeding and that is a very critical trait that leads him ultimately to success.***
