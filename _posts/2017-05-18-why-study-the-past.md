---
tags:
  - perspective
image: markers
---

The present and the future are mirrors of the past. While they are not perfect replicas, they all bear the same signatures. The present and the future can be altered by observing the past and applying its facts to the present. Just like the present is  a product of the past, the future could benefit from the present. In this way, the past stands between the present and the future.
> 
**_ "To be ignorant of what happened before you were born is to remain forever a child." - Cicero, 46 BC" _**

An intimate examination of the past can help shed light both on the present and the future. Often people are caught off guard by circumstances because they have no reference from the past. 'Caught off guard', is of course an understatement - but nonetheless starts to paint the idea I'm getting at. Many at times, people make shallow comparisons, hence they end up at a disadvantage anyway. Wise is  the man or woman that knows the following secret.

*Human beings are predictable; they elicit precise patterns. One only has to know what the markers of these patterns are, and then act accordingly.*

The weight of this truth transcends it. Not only do the markers exist, but also actions (responses) are neatly articulated in the stories and analogies of history. Some were genius, some were horrible. I must, however, qualify that the contents of the genius responses are not to be applied verbatim. Clever students of history know that dispassionate replication of situations beats the point of learning. Instead, learning requires of the student to apply the knowledge (in this case, patterns) to their time and circumstances. Similarly, there is something to be learnt from the poor responses. After all, some would have been genius absent of erroneous application.

Often you will hear the question along the lines of 'who cares about the past?' or 'What is so interesting about historical events or figures?' These questions emanate from very genuine concerns. It is much easier to concentrate about current affairs anyway. Or at least someone will argue in favour of bothering ourselves only with matters that are important to us at the moment, or in the near future. Genuine concerns but nonetheless wrong. 

Remember the old comparison how 'foolish men learn from their experiences' vs 'wise men who learn from the experiences of other'? Well, I take the cliche to mean that wise men have the added advantage of learning from other people's experiences. Unlike the man who learns only from his own experiences. To squeeze the most from this proverb, one has to understand that 'experiences' encompass the totality of context and outcomes. It is by no means limited to bad circumstances and ill outcomes.

The whole point of learning from others is to avoid experimentation, gives ourselves a head start, and to broaden our creativity. There is no doubt that our imagination and intuition can work wonders for us. My argument, however, is that aided with more information, our minds are capable of pure marvel. For these particular reasons, I go out of my way to look into the history of things I'm interested about. 

We are born into societies where we're fed certain 'truths' right from a very young age. We are essentially asked to hold on to these truths as foundational, immutable and sacred. Few as they may be, some of these truths are indeed foundational. Among such are family, patriotism, regard for human life, self-reliance and interpersonal decency. Some are intrinsically immutable depending on whom you ask. I will, therefore, argue that they seem to be subjectively immutable. For example,      I recently came across an academic professional saying that [there is no such thing as 'biological sex'](https://m.youtube.com/watch?v=10fDRERJh4w) - the kind of stuff that makes me wanna throw up. So just we are clear, I believe biological sex is a foundational truth - at least at this point of human evolution. That said, I believe that it is important to exercise tolerance towards transgender persons and all LGBT fraternity. 

It is very easy to be drowned by dogma, convention and common knowledge in a way that cripples us. Our ability to become the best versions of ourselves slowly fades. Worse yet, what we've been told, or are constantly bombarded to live by might leave us confused, drained an imprisoned. The way out is to seek the answers for ourselves. The study of the past is the best way to go.

From my personal experience, as a student of history, I realize that there's always more to every story. Studying history is more than just going through historical text. In between the lines are saturated markers of human psychology and foundational knowledge. It is clear that while our civilization has made advances in certain areas, it has also eroded a host of other foundational aspects of our society. The irony of it all is that we have ignorantly come to look at ourselves as smarter than our ancestors - a big mistake.

In our blindness we walk with an air of arrogance as though we have everything figured out. This level of ignorance is heart breaking. There is so much to learn, so much, that a hundred lifetimes wouldn't exhaust the information there is already. The thought that there is more undiscovered and unexplored history, in spite of the fact, is mind boggling. Yet all things considered, a few years of examining historical texts is invaluable. One learns so much about where our society is coming from and the various personality types that make our society. 

There is a lot to write on this topic that it is kind of difficult to squeeze it all in a concise and elaborate manner in one post.

To be continued ...
