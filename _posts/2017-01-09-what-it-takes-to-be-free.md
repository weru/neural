---
tags:
  - perspective
image: free
---

The feeling that you are not as free as you would like to be is really unnerving. No animal on earth wants to feel helpless,
and certainly not we humans. We all crave real independence. Being in control of our income, expenses, relationships, choice
to engage and not to engage. Often at times, those wishes remain that way - there is only so much that we can do. Or so it seems.

While our wishes are lofty and seemingly simplistic, our actions or rather, the lack thereof, are often wanting. Worse yet, 
our beliefs are always self-defeating. We hope for a grand time in the near future when things will miraculously fall into place and wish that our circumstances will take a turn for better. 

We are our own darkest enemies, we set the standards for ourselves so low. We paralyze ourselves in fear, self-doubt and the inevitable inaction. Clinically self-guessing our intuition, our guts - we don't believe in ourselves enough to bet big on ourselves. Like a dog or an elephant, we have found our way into learned helplessness. The chains, once strong in our helpless seem immovable - only because we gave up trying. 

The truth that eludes us is that we have become better, could become better, and the chains have become rusty. That knowledge is priceless.

As a result, we look out for 'heroes', our government, our politicians, our parents, our 'connected' relatives, our self-made buddies. Why not? After all, they owe it to us to give back to the society, to support us, to guide us, to create jobs, to lower taxes, to increase welfare funding ... there is really no end to the yapping. 

Do we hear ourselves? We are a deplorable generation, drunk with entitlement. Too timid to step up and venture out into the wild, into untested fields. A soft generation - one wonders what the world of tomorrow will look like if we're to be its parents.

We love what is easy, direct and readymade. We consume more than we produce, take more from the society than what we give back. Our egos hurt when we are reproached for our inaction. Like a boy who hides behind his mother's skirts when confronted with the prospect of battle or harassment from his peers, we hide ourselves behind realism.

'No but realistically' not everyone can afford decent housing, not everyone is privileged with a good education, not everyone
can secure a safe job, not everyone can be an entrepreneur - not everyone is as smart, lucky or talented as you. Whining!

The list of 'realistic' improbabilities is as long as our ever 'realistic', ever timid brains can come up with. All in an attempt to defend inaction, a way to 'honorably' save face, as if there is any honour in reciting excuses.

A large portion of human kind has pissed on its imaginative and competitive potential. All we are good at is consuming, comparing, analysing and tearing others apart. It's no surprise we live half of our days on social media, not really socialising but showing of and sharing fluff with strangers.

Take a moment and think - if we literally did what we do on social media in real life, many of
us would come out as creepy, insecure, and shallow. In fact, some of us would be on the authorities' watch list. When one would expect us to be getting closer, benefiting from the vast information about human behaviour, we end up being anything but. The truth is that we hardly know ourselves, poor judges of character and frustratingly shallow in our conversations.

Young people, instead of investing in ourselves through books, we are constantly looking for the next picture, comic videos or mindless rants to like. Young women are busy with make up, selfies, and filters to showcase their anything but psyches trying to keep up with their friends and celebs. Young women? Damn right, they particularly excel in this madness, but it is a shame that almost a proportionate group of men are following suite. The [losing men](/a-losing-man/) pack continues to swell.

While I understand that it's in women nature to be vain, it is not hard to see why so many of them
are so annoying these days. There are so many of them with an inflated sense of self-worth, with little to no value once they start talking. Little girl, please - try to cultivate some depth first. No one is free until they can at least see past their delusions.

Not even a million likes from Instagram can drown your insecurities, your vanity, your need for external validation. Not especially when the object of those likes is a half naked, superficial images of you. And cool - young men it is ok to follow that ass. For hells sake though, don't waste an hour of your day distracted by inflated meat; unless you pride yourself in being a meathead of course. 
There is still plenty of good, less superficial stuff out there. Mind to build your value and the world will be yours for the taking. The old cliché that *'time is money'* is timeless. Apply yourself to [value time](/valuing-time/); it's sacred.

A man ought to have his own mission, and no woman, lady or whore should distract - only help ought to be welcome. Majority of 
them have become fickle anyway. 

So how then can we truly be free? Is it attainable? Yeah, it is in every sense of the word - not for everyone though. It depends on what one is made of. For the mentally resilient folks, who understand that freedom is never free. One has to work their way into it. In fact, you must want it so bad to endure the process. There just so many distractions on the highway to freedom. Social norms, laziness, fear, despair, fatigue, gratification, [lies](/lies-we-progate/). You get the idea.

The bottom line is that **freedom begins in the mind. It is a choice to live by one's own code, to set the rules and standards by which one will and must operate.** However, one must know the boundaries - or their quest will be self-defeating. I once heard that wisdom is knowing what one knows and what they do not. That was profound. A wise individual, therefore, always have an idea of what they are, whom they would like to be, and whom they would hate to be. Where they are unsure or clueless, they apply themselves to seeking out the unknown to better their odds.

Such individuals, thus, have no problem reading books, magazines, blogs - whatever they have to read to milk the information they need. They love themselves well enough to know that they could best look for individuals who are better than them. Who can challenge their ideas, perspectives and character. 

They are also their own persons. They know when to say no and more importantly, they understand the value of action. For this 
reason, they despise inaction, because it is often a waste of time - the very substance life is made of.

Through the [quest for knowledge](/self-education/) and their esteem for action, wise people have an equally high regard for breaking the rules, being different. They understand the limiting ability of yearning to be average, normal and lazy.
