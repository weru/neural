---
tags:
  - perspective
image: line
---

Sometimes last year I wrote a piece, [lies we help propagate](/lies-we-propagate/). It was hardly an exhaustive list - only a couple of things that came to mind at the time. This piece adds to that list. 

The truths of our days can be quite annoying, all the reason why it is less desirable. The glaring conflict being weighed between *what is* versus *what ought to be*. It's always desirable to aspire to achieve *what ought to be*, wise to stand on *what is*, but wiser to determine the cost of living outside *what is*. 

Seeing *what is* helps us underline the lies that surround us. There is an unspoken notion that we need to toe the line to accommodate the realities of those who were once kind to us. That we owe them no matter what ... hence we must in thought, temperament, and action accommodate their expectations of us ~ indefinitely beholden.  To which I say, screw that!

Else we will buckle and become someone who we have all endured. A man who seeks out to make the highest number of people happy; especially at his own expense. He is like a man cursed to persist in his folly. Yet unlike [Sisyphus](http://dbanach.com/sisyphus.htm), I'm not convinced that the gods have anything to do with his turmoil.  

He is a victim of his own delusions. Primarily, his ignorance and pride constitute his folly. Unknown to himself, the world walks all over him as he tries to be a hero. And a hero he thinks he must become. Sadly, unlike most heroes, who are really dead, he still walks among us - lifeless. 

In what manner is he dead? He actually believes that he has something to lose if he doesn’t tend to other people’s fickle whims. If only he knew better. If he did, he wouldn't bother himself with the business of heroes. At least not in the form of martyrdom. We know for a fact that unlike real heroes, martyrs are hardly ever celebrated. At the dawn of personal enlightenment, every man must learn to be a hero of their inner world, before they can entertain the luxury of being other people's hero. Enlightened self-interest.

There is a breed of individuals among us who make it their business to ruffle our feathers. It is almost as though a smile on our faces produces an itch up their cracks. Unfortunately, these are the people who have helped us at one point in our lives - family and friends. 

By that token, they make it clear that they are entitled to our obedience, company, help. That we're not only obligated to sit back and let them squeeze our nuts for their personal validation but that we must also be grateful for the shit they put us through. Like a dog that leaks it owner's feet right after he flogs it. Hmm, even dogs can only entertain so much before they give you that look that says 'screw you', right before they skip the neighborhood for good.

It puzzles me why we tolerate this kind of abuse. Is it because we assume that their criticisms are based on fact; like we're expected to? That they emanate from the noblest of intentions? Or that we will be deemed disrespectful if we don't pay attention? 

They say *"the best way to destroy a person is to make them afraid of making mistakes."* And what better way is there to achieve this than to taint a person's conscience with guilt? Make them feel like they have failed as friends or family because they are supposedly biting the hand that once fed them.

"How can you? No, how dare you?". After all I have done for you? Notice that these words need not be said, though they are often said. When those who would say them cannot summon the balls, we almost always recognize that funny look that feels like, “You know you owe.” Crap, don't say! And ain't this kind of shit heartbreaking?

Let it not be said that I’m opposed to people showing gratitude, nor I’m encouraging us to shamelessly piss on the kindness that has been extended to us. That said, it occurs to me that we need not be held captives unnecessarily by those who have been good to us. To me, it appears as though gratitude ought to be mindful of extent, and context. 

Selfless acts come in many forms, most of which are selfish. In fact, you've also heard, *behind every selfless act is a selfish motive*. Individuals will always ask for more in return than they did for you. If you disagree, do you think the concept of interest was first introduced by banks? 

Individuals will delude themselves that what they did for you was to your benefit. The same bankers tout their loans as enabling entrepreneurship, while in fact, they couldn’t give a shit about your empowerment. If you examine why they 'helped' you, altruism comes at the end of the list. It is more probable that they did it because, they could, for their personal gratification, and as an investment into your future reciprocation. Of course, these motives are quickly weighed in the subconscious. While they are not necessarily bad, people fail to recognize when they go too far.

Assuming that a kind act done in my favour is an investment to compel my future compliance, it is necessary that the scale of my likely compliance be realistic and where possible proportional. This way, we'll both know that my gratitude is not a tree that keeps giving. 

> In fact, given that you exercised personal gratification, you should not dictate the terms of my gratitude. *Should a kind act not be its own reward?*

Perhaps, yesterday's givers should be careful lest they fall into the frenzy where they have an inflated sense of self-importance.

I have reckoned that it's a dumbass idea to reason by my heart since the heart has no brain of its own - the brain will always do a better job. Examine the facts in your mind, then draw a line in the sand should they fail to. No one should be entitled to anything from us just because they have an itchy ‘you owe me’ feeling stuck in their .... 

Yes, you are grateful for their kindness, but that does not mean that they have any form of control on how you think, choose or live your life. Ain't that right? I suppose that is the sort of attitude that will rub some people the wrong way. Hmm, I suppose you wonder why, haha.
