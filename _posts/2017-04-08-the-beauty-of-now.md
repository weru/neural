---
tags:
  - perspective
image: now
---

So much has been said and written concerning the value of living in the now. Nothing beats having a good time with friends.
Even then, it is not the presence of friends that solely brings happiness. It is being at peace with yourself that really 
allows the magic to happen. When we are at ease with ourselves we get along with ourselves and as a consequence, by others.
The reason why there is so much suffering is because many of us are in a state of constant struggle with ourselves. Why is 
this so? We’re either too hard on ourselves or are ungrateful for the ‘little’ we have.

As I grow older, I have widened my knowledge base. Yet the more knowledgeable I get, the more I discover how little I know. 
This realization is as liberating as it is humbling. It is humbling because I realize that in the grand scheme of things,
everyone is just as great or irrelevant as I am. It is liberating because it teaches me not only to be grateful for the 
‘little’ I have, but also because if I can do the best I can, that will be enough.

Upon further examination of my past, I realize that most of the pain I have experienced was unnecessary at best. Unnecessary 
because I created it. Most of it has been a product of ego; the false self. Every time we use ego wrongly, we ignorantly step
into minefields of our own making. We set up ourselves for a world of hurt. Not to mention the list of victims we create in 
the process.

Recently I was watching a show about the cosmos, **Cosmic Odyssey**. When you consider the size of the universe, the very
vastness of our galaxy among trillions of other galaxies, is humbling. Our sun is just a grain in the pool of stars. Even 
though our planet does indeed look special, that assertion is provisional at best. Something yells that we are not the only 
ones in the universe. Too bad our mortality may not allow us enough time to find out.

My argument is that there is beauty in living in the now. As long as we've known, we’re mortal beings, living at the pleasure of time. Even wishful thinking gives in to the reality of our own mortality - eventually.
All we have is a fraction of it; a fraction so small that in the context of time’s history we seem irrelevant. If our lifetime
as mapped into onto a map, it would like a dot so small to identify with the naked eye. 

If then we're that irrelevant in the context of time and space, does that mean our lives are good for nothing? No. Our lives are an incredible honour. As irrelevant as we may seem, in the cosmic context, our lives mean a lot in the context of our planet and of now. Earth is the only home that we have ever truly known. Today is ours because we have it. The past has eluded us and the future has no guarantees for us. If only we could travel back and forward in time.

The now is ours to embrace, honour and be grateful for. If we have nothing, we’re considered lucky to exist. When I look at
the infinite nature of our universe, at the diversity of the human race itself - something tells me I didn't have to exist
for galaxies to form, nature to self-direct or for humanity to thrive. Yet here I’m; afforded a chance to make an atomic mark
in the infinite fabric of time. That's why our lives are a blessing folks.

When I look around, everyone strikes me as special; precious even. Even the majority of us who in words,disposition and 
actions are anything but. Even the dogs that dies on the streets or the ants I step on as I walk around. Everyone and 
everything that exists in the time that I live. Everyone and everything who are part of our reality, whether we're conscious
of their existence or not, gives us a reason to be cheerful. How might we show our gratitude? By actively trying to be the
best version of ourselves. It is a progressive endeavour to be battled out in our lifetime.

Since our lifetime is so small, every day should count. For every day to count, ‘the now’ must be worth something. There is
beauty in the now. **Greg Plitt** said, *“You only live once, but if you work it right, once is enough”*. How then might we work it right? Let's treat ourselves and each other better. Let’s correct and celebrate one another. Let's nurture our talents and potential. Let's us be the best version of ourselves. Let's illuminate the beautiful of now, our now, and let it shine.

Maybe we should awaken to the reality that we will not always be special. Every thing comes to an end - even stars die (after billions of years). The now is limited; and so are our chances. Some of us may have already ceased to be special. Some reversibly, some irreversibly. That thought is sobering. Once we are sober, we can contemplate on how we might be at peace with ourselves and those that grace our finite experience. 
