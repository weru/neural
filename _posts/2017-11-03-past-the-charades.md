---
tags:
  - perspective
image: charades
---

They say the breadth of experience serves a man well. Following last month's events, I couldn't possibly agree more. Experience is definitely a teacher to the willing and unwilling. The French say *Qui vivra verra*, He who lives, shall see.

 Regardless of whether or not the lesson(s) is learnt, the teacher always is there. Think about it, who can rightfully divorce a teacher from their title merely on how well or badly the students fair? Surely, there are great teachers, shitty teachers and many variants therein. It's sad that often a teacher is defined by how well or badly the subjects they act upon fair. Sad.

October was an interesting month for me. It wasn't just an interval in the continuum from the future and the past. It was the confluence of the present, the past and the future. When you pull back the curtain, seeing what your eyes have long failed to see is immensely mind blowing. It is like scouting the brushes of a painting for the first time. Realising the genius of the artist. The purpose behind every brush stroke. The realisation, that even in their effort to correct a flaw, there was intention to justify the action. That there is value even in the omissions. Not everything that interests the mind need be said nor recorded. 

I need not bore you with the wandering of my thoughts.  For that reason, I will be quick to get to what I meant to say. Behind (or need I say in front) the curtain lay the reality of my world, our shared world rather. In the midst of our shared world stood a  dazzling facade. A front that tricks most people in their sobriety. In the real sense, the facade is not dazzling; it is only so by our laziness to observe and wanting sense of imagination. 

> Maybe it is only once you've been onto the other side of the divide that you can see the whole picture. And who is to say that that is all there is to it? Second, what of those who cannot see, no matter where they are? 

Let it not be said that I'm refering to physical disability. If anything, everything that is physically visible is just but a tip of the iceberg. Where I hail from, there is a sarcastic jest that is goes like, *we read the letter in its envelope*. It's a self-congraturatory response, meaning one can tell beyond the apparent. There is a depth to the things we cannot see. Those things that need a knowing beyond sight. A knack to decipher meaning, intentions, motivations ... [games](/may-the-games-continue/), remember? 

I'm currently reading "The obstacle is the way" by Ryan Holiday. From what I gather, things can exist or fail  to exist purely based on perception. Two, nothing is purely good or bad. While there maybe a sense of exaggeration to those words, I nonetheles sense a lesson, wisdom perhaps, in there.

After what I have seen and heard, I have no doubt that I know not of the world I find myself in. The more I learn the more I see my folly. I will therefore listen and observe more. More importantly, I will trust the nudge when my instinct tells me something is up. Sometimes the nudge will be wrong, but I will get gist of this. That way, I might see past the charades.
