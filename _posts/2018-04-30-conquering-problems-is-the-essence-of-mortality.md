---
permalink: /conquering-problems-is-the-essence-of-mortality/
image: conquer
tags: 
  - perspective
keywords:
  - 'problems'
  - 'mortality'
  - 'conquer'
  - 'life'
---

We live interesting times. At times, it seems as though nothing can stand in the way of human civilization. We have come so far in terms of technology that the future prospects are mind blowing. 

There is so much to look forward to as there is stuff that scares the shit out of us. An AI revolution, a nuclear holocaust, a biological or chemical warfare. But then again we have always had some things to scare us throughout history. Of course, in our case more is happening. 

We have always found better ways of doing things so that we can solve the problems of life. And what are the problems of life? Anything that we perceive as hard, lethal, inconveniencing,sub-optimal, limiting ... You get the idea. Each one of us have a list of problems that they need to solve. Again, some more than others.

Normally, when we hear the word ‘problem’, we almost certainly paint a negative picture in our minds. Fact is, every problem isn’t necessarily a bad thing, not if it is solvable. We spend time, the essence of mortality, moving past problems ... conquering. Beyond every problem we conquer is a reward ... the satisfaction of having prevailed and reaching a new bar. 

When we start thinking that we have conquered all problems the passion we derived from the quest slumps. Unfortunately, at this point our lives start going down. All sort of bad things happen to us. Be warned, tragedy doesn't come suddenly, thus it is more sneaky. Some of us start gaining weight in all sorts of funny places, lose our vitality ... when you really think about it, it’s as if our lives and world start shrinking. Our moments gradually become unproductive and boring. Predictably, all types of dis-ease-es - physical, mental or spiritual find a safe haven in us.

Ever read the parable of the rich man who filled all his barns with harvest, only to be cursed to die on the night he thought that he could retire from his labor’s? 

There is a scene from the show *walking dead* that I like a lot. And yes, I have watched that *zombie show*. A bit of background for those who might not have watched the show. The show is about an apocalyptic disease where everyone on Earth is infected with a virus that turns you into a zombie once you die, or get bitten by a zombie.

Over 90% of the population has turned into zombies and the last of living have to constantly watch their backs. Problem is, the zombies are always lazily hunting the living. While the threat from zombies is sufficiently damning, the living make things worse by ambushing each other as they fight for control over what is left of the world. So the threat is from both the living and zombies (a.k.a, the walking dead).

The characters in the show suffer very tragic losses among their ranks and they’re unable to stay at one place for long due to the aforementioned threats. In fact, every time they achieve some form of stability, something bad happens and some of their members die. 

Skipping forward to the scene, there’s a teen girl and a woman in her late forties. The teen says, *"I’m bored"* and the woman says sarcastically *"If you’re safe enough to be bored, you’re lucky."*.

To be wise is to learn to enjoy the toils of life, the brief moments of accomplishments that follow a conquered problem and then embracing new challenges. 

Life ain't fun just being. To really live is to continually yearn to conquer, to demolish what's subpar and create new and better realities. It's to constantly expose oneself to what's uncomfortable and intrinsically risky. This is what it means to chase glory in the arena of life having unearthed its secrets.

Ergo, it goes without saying that to discover these truths is to discover the basic truths of life. That knowledge itself being a safeguard against nihilism and it's corrosive and self defeating agenda.

Let's not kid ourselves, we will not solve every problem of life. Hell, there are problems that are just beyond our time. For this reason we must learn to pick our battles. Sometimes, *it is what it is*. Some things are just gonna have to be. But at least we know better than to whine about how much problems won't let us be.