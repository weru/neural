---
title: Joining Software Developers' Fast Lane
image: fast-lane
tags:
  - tech
keywords:
  - tutorials purgatory
  - accomplishment
  - software developer
  - programming
---

> Very few things in life beat the joy of accomplishment. In fact, I'm persuaded that virtually everything in life worth celebrating must at its very core be the embodiment of accomplishment. It is against this backdrop that we then must understand the grief of a struggling wannabe software developer.

It's really heart breaking to witness the once blistering passion being slowly and painfully strangled by doubt, misdirection and the eventual despair. This kind of agony I understand all too well, for I have been there.

![frustrated and worried](/assets/posts/worried.svg)

Surely, feeling like you have faithfully been failing yourself is a constant vexation of one's soul. This annoyance is like an owl inside your head that keeps repeating that dreadful omen.

When I was starting out as a developer, my thinking was inspired by the famous woodsman story. In the story, **the gentleman is asked what he would do if he had 5 minutes to chop down a tree. Wise as he was, he replied that he would spend half of the time sharpening his axe.** 

There's no doubt that there is a powerful lesson from the story. That said, I now realize that in software development, the lesson isn't applicable in the sense I thought it was … not by a long shot. 

Whenever this kind of reasoning is applied, a lot of time is mindlessly used to pussyfoot around with tutorials, stacks and podcasts. Before one knows it, they are deep in enemy territory. That territory got itself a name, **tutorial purgatory**. 

### Tutorial purgatory
Remember [my friend who's still putting together their development environment](why-you-may-never-become-a-developer)? Well, he's about get deeper into the forest of unending tutorials. 

Often, lots of folks are reluctant to make the jump into the active process of writing code and building projects because they want to go through as much content as they can. In reality, this stage doesn't always feel as bad as it sounds. 

The experience of watching a good tutorial series is just as satisfying as the period when an addict is on whatever they fancy to indulge in. It really does make you feel like you're not only learning a lot but also as though you're gaining an edge over other people in the industry who are not that knowledge thirsty. 

> However, like the high of a drug, this feeling of accomplishment lasts only as long as you're on the tutorial series.

The agony begins during the breaks from the high. Once you're on your own, you often realize that most of what you felt like you were learning has either evaporated or it's just not good enough to justify the jump. This feeling of 'inadequacy' will almost always leave you wanting for another *'fix'*. What could be better than another set of 'better' and 'more in-depth' tutorials? As time flys by, the thought, "At what point is the accumulated knowledge good enough?" It gives way to other question … most of which are not encouraging.

I'm no expert on the concept of the underworld/purgatory. That said, I have heard the notion that there's a special depth(s) in that realm where souls trip upon and get trapped permanently. I assure you, inside the tutorial purgatory, such depths are as real as the air we breath.

### The case against dependence on tutorials

The age of internet has made it easy for us all, irrespective of our industries to get information we need to cultivate a skill. Tutorials are an integral part of this observation. I must qualify, however, that I have nothing against tutorials.

In my mind, they spice up learning ... they are like spices or dessert. In that stream of thought, it is then obvious that one cannot thrive on a meal of spices and dessert. Their health would most certainly be compromised, wouldn't it?

In my estimation, tutorials can fit into one of the following descriptions

#### 1. Some tutorials show you how to complete a subjective task

Instead of teaching you the general procedure/principles of developing a host of features, most tutorials will show you how to copy verbatim the process of building a specific feature. You obviously will almost always not want to do this. Often, this is not a matter of choice.

The danger in this approach is that it limits your ability to replicate the same knowledge to build other features. Of course, this doesn't have to be the case. If the tutorial takes some time to cover the underlying principles, the results could be better.

#### 2. Most tutorials try to bundle a lot of concepts in a single set

There's a limit to what our brains can absorb at once. This observation is particularly true when we're learning new skills. Over time, the mind has evolved to grasp concepts in incremental bits, slowly but consistently.

You can, therefore, imagine the futility of having to learn over half a dozen concepts in one seating. No doubt, we may find that one or two concepts that stick. Be that as it may, most of the other concepts will evaporate almost immediately. 

The point here is to illustrate how inefficient tutorials could end up being. More so, if they are the primary material we depend on or they're the first material of its kind.


#### 3. A large number of tutorials are sketchy at best

![angry](/assets/posts/angry.jpg)

Since most tutorials try to cover a wide scope, a large proportion of them are rather shallow. This is even worse if the author or the one presenting the tutorial is either ill-baked in expertise or they're just awful at teaching. Sometimes it is both.


In spite of their limitations, tutorials remain much relevant in acquisition of knowledge and skills. In most cases, the degree to which they help is dependent to how well a person is able to select. It goes without saying that there are a lot of really well-crafted tutorials. There also a tonne of lousy ones. When selecting, most of the times it boils down to sheer luck. 

If one is smart though, a little due diligence helps a lot in avoiding the pitfalls.


### A better approach

Instead of wandering blind in the tutorial purgatory, one could use a better approach. It is a way that is akin to erecting a building upon a strong foundation, which is unsusceptible to erosion and wind. Which is this way?

*Learn the basics of your niche as first as you can and start building something that interests you soonest possible.*

### Where and how do I learn the basics?
Look for good books, courses, mentors and wholesome tutorials. A lot of this comes down to good research and networking. If you find a good mentor in the industry, they will save you a lot of pain. That said, you will have to rely on yourself most of the time. 

> Good research is indispensable to a world class developer. It's becoming a cliche saying that **Google Search and developers cannot help but be first class buddies** ( Bing and Yahoo Search ... oops). A number of other things are part of that friendship. This include, stackoverflow.com, documentation, youtube.

The beauty with this approach is that it saves you a lot of time and the probability that you will get stuck in your own head. You will not concern yourself with the need to be safe about variables you do not know about.

The basics are important because they give you the confidence that you understand the fundamental elements of your art. Everything else in your journey will be built around these concepts. Virtually every advanced concepts will be composed of these basics.
Make no mistake, the goal is not to learn all the basics available. All you have to do is achieve the base minimum of basics under your fingertips to get you up and running.

Programming has often been defined as the process through which developers solve problems. Every developer worth their salt will tell you that the creative process is an iterative process. Primarily, writing code is the process of creating a product that is initially marred by mistakes, bugs. 

> It's probably why when we revisit old versions of our code we cannot help but chuckle at how far we have come. 

If you are an aspiring software developer, start putting together that seemingly small or ambitious project you have in mind. Don't worry on how ugly it might look, for tomorrow you shall revisit it and make it prettier and less buggy.

> You'll be better writing subpar code than not not writing any code at all.

### Learn to share your work no matter how humble

Once you have built one or two projects, take pride in your work and showcase it on your portfolio. Chances are that someone will take notice and critic your work, share your work with people in their network or even want to hire you.

The goal here is to put your work out there and get some feedback.
![feedback](/assets/posts/feedback.svg)

### What if my first projects are really bad?

Surprise surprise! You're probably right, most of your first projects will really be look awful (I know mine were) … to seasoned developers; not necessarily to non-techies. Does it then mean that you should share your work with non-developers? Hell no, share your work with your potential mentors or critics.

When I think of a developer's first works, the experience of observing a baby comes to mind. A baby looks feeble (delicate bones, everything under-developed), helpless, they may shit themselves, make a mess of themselves if they grab a spoon and some food. At the same time, a baby fascinates me with the possibilities they represent. They could be a titan of my time, they could rule over us, they could be that person to propel the rest of us forward.

![fast laners](/assets/posts/fast-lane.svg)

To join the very best of software developers, the above rumbling should be insightful to aspiring or struggling developers. 