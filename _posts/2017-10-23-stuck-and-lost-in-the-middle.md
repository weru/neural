---
tags:
  - perspective
image: stuck
---

I was looking back, some stock taking, to assess how much has been. Out of what has been, how much has fallen away, how much has stuck. Of the stuff that has stuck, what is particularly unique. What can be learnt from all that which has been? Specifically, what is repairable, redeemable or what can be further optimized?

As I indulged my mind into the intricacies of what has been, I kept going back to people. Individuals who at one point in my past had caught my attention. Challenged me to hold myself to a higher standard. After years, it's only natural that I would be curious to know how they were fairing. Had they made it? What could I learn from them if they had? On some level, I looked upto them as some sort of mentors. 'Monkey see, monkey do', you get the idea.

In my early college days, there was a guy we kinda looked upto. Martin, is how we said his name. Not to say his life was exemplary, but little about him was cliche-like either. He was the man with a sense of drive about him. We thought we would have been lucky being him. There was something about him that oozed  hunger. The kind of hunger that gets things done. When he did shit, there was some sense of urgency in his being. 

A few years down the line, Martin's life is neither great nor mediocre. A quick look at the guy tells of a dream that has somewhat withered. He may still hum some wise tones, but his actions make his words sounds like gaffes. As you listen to him, there is a vybe of self loathing that he can't help but betray. There is no doubt that absent a strong force, one that might steer him off his current trojectory, he is cruising headfast into despair. It's just painful to watch a man gradually become part of the walking dead. A drowning man who is just about to stop trying. Limp from frustration and dispair, descending into depths of hell.

There is no doubt in my mind that Martin is truly a brilliant guy. He has great ideas on what he ought to do to make something of himself. Why then is he worse of than when I first met him?  Circumstances? Take a step back and consider. Almost every one of us has been spanked by life. Some more intimately than others of course.

The story of Martin is by no means unique to him. He is not the only citizen in a nation of frustrated and drowning men - stuck and lost in the middle. In my experience only a few people are living their lives as they had planned to. Sometimes I am at a loss at figuring out why that is. I mean, I have some ideas and it's not like I can ask them what went wrong. After all, a knowing deep inside of me knows that their answers would not be very different from the ones I could put forward ... asked the same question. They are just don't cut it. At best they sound like the excuses. Are they not?

### We have great ideas on what we ought to do

Excuses because all the factors we allude to are not in any way unique to any of us. If I'm right, which I'm, they are not really lost for what to do. Quite on the contrary, some of us are incredibly knowledgeable individuals, some extremely talented. Which means on some level we know what we need to do; we just haven't gotten around doing that which we know how to do.

Bob Proctor, author of ['You Were Born Rich'](http://www.proctorgallagherinstitute.com/you-were-born-rich-book) describes this scenario in a rather neat manner. 
> Superior knowledge and inferior results causes confusion and frustration 

### Remember that our lives constitute a game

In [one of my latest post](/may-the-games-continue), I likened life to a series of games. I argued that life is one grand game, where we have no choice but to play. If we do not, the game continues regardless. Since, our inaction neither invalidates nor haults the game, it would seem the only choice favourable to us is playing.

It is good to know that the game of life is not necessarily a 'catch 22' situation. We are not damned regardless. We're damned if we don't. Inaction guarantees failure, pain, tears and regrets. Doing rapidly improves our odds. When it doesn't, the knowing that we tried, is enough consolation. We are lucky that nature cares to the extent that we actively care. Thus, the possibility that our efforts fail to yield are abysmal.

### Awareness and the subconsious mind

This then brings me to the potency of awareness and the subconscious mind. How do they fit into the overall picture? Much of this territory has gone unexploded. Knowing its impact is incredibly enlightening. It's a pivot that makes our labours all the more forthcoming. 

Everything that has been has first been held in the mind. Then it has been manifested through unrelenting desire to have it so. The problem occurs when the thought fails beyond the rationale level. It has been always been necessary that one be unrelenting in their fantasy. This way, they can push through hell with the confidence that their dream will show up in reality.

We quit making decisions after life deals our goals several blows. We see no way out and feel so wasted to look for better answers. Essentially we ran into a snare inside our minds that begs us into the comfort of having not to care anymore. 

It's the realm of greatness that few have grasped. Many a great men who have transformed our world have touched on the subject lightly. Nevertheless with such precision. 

> Einstein once said, 'the intuitive mind is a sacred gift, the rationale mind a faithful servant, we have created a society that honors the servant and has forgotten the gift.

### We are really not alone

Finally it's important that we recognize ourselves as part of a larger whole. Sure, where it matters we must make major decisions alone. That said, we are part of a larger field of awareness that is in and of itself benovelent.

Therefore, whenever the weight of our inadequancies befall us, it is important to hit pause and seek help. At the most basic level, we start with those around us. Although most of them will not get it, and will be lost for answers, the very act of trying will be worth something.

The other thing we have to remember is that there is a knowing deep within us that keeps us alive. That keeps us safe, that knowing is the part of the benovelence that hold.
