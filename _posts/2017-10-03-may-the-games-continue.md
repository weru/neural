---
tags:
  - perspective
image: illusion
---
When I was young and naive (juvenile), I saw the world in black and white. My failure to see the world in its diverse colours, dimensions and forms hindered my capacity to see and understand the world as it is. Fortunately, my *infantil* nature had an end. I’m fairly certain that I have more than age to back that fact.

As I grow older I increasingly grow aware of my mortality. Most of the time I feel a hollowness surrounding my life. Every other second when I’m not occupied by some activity, conversation or thought. I’m scared that I will soon be swallowed by it all ... the nothingness. ‘Chasing of the wind’, a man reportedly wiser than us all once said.

The other day I took my mind to a cross country. Scanning for at least a name, one person that I could engage in a discourse; one of a personal nature. One that could perhaps help me ease the tension from the weight that weighs down on me. My mind came out not only tired but also empty. Blanks, no name, not even a clue.

You would think that it would be easier to find someone, a few perhaps, with whom you could reason from the same footing. Oh my, isn’t that rare to find. An individual that just gets it, a comrade in every sense; a kindred brain. I have come to infer that most people are unfit for such a position. Unfit as a result of ignorance of their higher and most profound faculties or the absence of such faculties altogether. Sometimes I’m convinced that the world isn’t barren. In fact, it might as well be that most of its occupants are  the barren ones in this sense.

Yet I refused to give in to the possibility that there’s really no name. At least not one that I know of; so I woke up from the bed where I lay. I walked to my coffee table where I prefer to leave my phone every time I go to bed. Perhaps my phone book could produce a name, one that my aging mind might have overlooked. And to consider the irony of it all, considering that my phone book is rather spacious ... if you know what I mean. Well, remember my occasional conviction about us occupants (most of us) of planet earth? Maybe it’s time I took the conviction to heart. Hmm, I don’t know, I should probably hold out for a while longer.

70, 69 ... down to Z and still a name did not come up. Man, doesn’t it suck, to wanna talk so badly and not identify a name you could trust not only to understand but also truly listen. An analytical and objective mind that has a sense of untethered imagination to free the mind ... making discourse all the more worthwhile.

That leaves me with questions, ones that there’s no one to answer. Not even someone to help me ask the questions. That very thought infuriates me. Questions, gaps, secrets.

If only I was somewhere in the middle of nowhere. Perhaps in the middle of the jungle, middle of the ocean perhaps. Somewhere where there’s not another human soul. A place to scream my guts out, get it all out of my system. And wallow in the guarantee that there's really no name. Questions, gaps, secrets. Oh and illusions.

Most stories that you and I have heard are told in a manner that in the very least infer an anticipated ending. More often than not that ending is skewed to sound like a preferable destination of sorts. Our tendency to simplify everything is, well, mind boggling. Isn’t it possible that some stories, many stories even have an abrupt unforeseen ending? Stories whose mystery is in their abrupt advent and untimely demise.

What is it that we call life? Reality?

From what I have heard, seen and read, life is a series of iterative games; which are in fact part of a large game that only the universe or divinities understand. Few realize that it’s even a game. Half of those who recognize it for a game hate to admit that reality. So they bury their heads in the sand with the rest of us who know nothing of it. In retrospect, it would be unbecoming of us to be hard on them. Chess is a game that most people can't stand, right? Umh, life games are crazy complex compared to chess. Ignorance might be bliss after all.

Fortunately, it helps  if and when we identify the games as they happen ... see life for what it is. Or at least the rules of engagement. Before we were, others participated in the games and faired differently. If we’re willing to learn, we’ve some sort of a cheatsheet to help us with the questions and gaps. Probably even a little with the secrets. Hoping there are plenty of rounds to go, yet. May the illusions collapse upon their walls too.

Back in the days of old, during the celebrations at the Roman Coliseum, emperors would proclaim, “May the games begin!”. Today I join in on a different platform and say, “May the games continue”.
