---
tags: 
  - tech
image: speakers
keywords: 
 - 'Smart speakers'
 - 'Alexa'
 - 'Siri'
 - 'Google Assistant'
 - 'Amazon Echo'
 - 'Google home'
 - 'Homepod'
 - 'Sonos One'
---

The past two decades has been about bringing the internet to the consumer's pocket. The other objective of this endeavor has been to load as much computing power in individuals pockets and palms as possible. With good reason, it seems even as smartphone get more capable, this computing power is really never enough.

> Most people would be surprised to find out that the average smartphone is tens (others hundreds) of times more powerful than [the computer that first took as to the moon](http://www.computerweekly.com/feature/Apollo-11-The-computers-that-put-man-on-the-moon). Pause for a moment and ponder over that fact.

Obviously, we have come a long way. Given this advancement in technology and competitive spirit in the tech sector, we can still pack more computing power on a smartphone. I truly think that things are only getting better.

While that is one facet of our lives, there is more that can be done to push the edges of what we can experience. Now that we have technology right in our pockets, it's kinda cool taking things a notch higher. How about enabling us to have the power to have this computing power on our lips - voice computing? What if we do not need to manually interact with devices to fulfill menial tasks such as playing music, quick browsing, texting, calling, setting reminders? This list just got us started, there is much more you and I would rather say and have it happen.

In the last half-decade or so, there has been a market segment that has been slowly growing to fulfill this need. Smart speakers are now taking the tech industry like a gradually gathering storm. When they first hit the market, the concept did not resonate with most people. After all, the technology at the time was quite juvenile it was just hard to draw people in.

Today, in 2018, the technology has gained considerable momentum, and the products have somewhat matured to provide the sort of value that we could all use in our homes.

When we hear of smart speakers what comes to mind is music. Good speakers have always been built with music in mind. And that is not surprising given that we love music - all of us in our own way and form. 

However, smart speakers about are much more than just music. [Home automation](https://www.cnet.com/news/smart-home-buying-guide-home-automation/) and [voice shopping](https://www.usatoday.com/story/money/2018/02/28/alexa-need-everything-voice-shopping-becomes-common-sales-through-amazons-alexa-others-could-reach-4/367426002/) are the next big things the smart speakers industry will strive to facilitate. 

### Who is in the market?

The are over a dozen smart speakers in the market. However, only a few of those brands have managed to stand out.

#### 1. Amazon Echo
![Amazon Echo](/assets/posts/amazon-echo.jpg)
Amazon Echo leads the pack in terms of market share. Amazon echo dominance in the market are due to its early entry in the market, low prices and its capable digital assistant Alexa. In fact, Amazon has a couple of options for it Amazon Echo thus giving its customer base a choice when it comes to what they can use. The thing that bothers me about the echo gadgets is that they sound crappy. That said, Alexa prowess in handling a range of tasks makes up for the otherwise mediocre hardware.

#### 2. Google's Home 
![Google Home Max](/assets/posts/google-max.jpg)
Google Home speakers are second in line in market share. Like Alexa, Google Home line uses its own Google assistant to answer users' questions and perform user requests. Although Google Assistant and Alexa are arguably close rivals in terms of performance, Google Speakers have better sound quality overall compared to Amazon echo devices. In terms of price Google starts as low as Amazon with its Google Home Mini, but has a premium option, Google Home Max, that sounds much better. It is the loudest smart speaker at full volume. The drawback with this product, however, is that its sound is not as great at full volume.

#### 3. Sonos One
![Sonos One](/assets/posts/sonos.jpg)
Sonos One gives you the best of both worlds. It's arguably the third best sounding smart speaker after Apple's homepod and Google Home Max. Two, it supports both Amazon's Alexa and Google Assistant as its smart assistant services. This way, Sonos One produces a wholesome experience that is hard to find in one speaker.

Like Amazon Echo, Sonos has been around for a long time, but up until lately, it lacked an inbuilt voice assistant. Finally, Alexa and google assistant are available to Sonos.

#### 4. Apple's Homepod
![Apple's Homepod](/assets/posts/homepod.jpg)
Apple's homepod was released on February. Put simply, it is just a sound engineering marvel packed in a rather compact and premium-looking body. This device is simply built with music in mind. The speakers sound great in whatever volume range without the slightest hint of distortion. 

When listening to music on the homepod, the vocals are emphasized while keeping the instrumentals neatly calibrated. On top of that, the speaker has spatial awareness, meaning it can tell how far it is from the walls and individual objects around it. It then uses its inbuilt Apple A8 chip to channel the music in a way that delivers a consistent experience from anywhere across the room.

It's pretty obvious that Siri is kinda dumb compared to Alexa and Google Assistant. That said, for the things that Siri can do, it does really well. Ultimately, Apple homepod's limitations are software oriented and due to Apple's strategy. I hope that Apple revises their approach and frees the homepod from the confines of Apple ecosystem and of course improve Siri capabilities.

### Minimum basic requirements for smart speakers

Smart speakers perform most of their functions on a search basis. For this reason, they are fully dependent on internet connectivity to function. Low bandwidth just cripples their capability. 

Therefore, adoption of this technology will be limited to customers who have a reliable bandwidth. In most of the cases, such customers will be in developed countries with a small proportion coming from developing countries.

The other factor is the level of advancement in digital assistant. After all, the entire purpose of a smart speaker is to deliver seamless voice control, search capabilities and automation.  

### Road ahead

It still bugs me that most smart assistants do not support follow up queries, hence limiting what you can do with a smart speaker. Google Assistant does support follow-up questions, but requires you to keep repeating the invocation 'Okay Google.' That said, that is pretty decent given that the others cannot do what Google can do in terms of follow-up. Sure, saying 'Hey Siri,' 'Google,' 'Alexa' may sound cool, but using those invoking phrases everytime you want to ask a new question gets pretty boring fast.

I wish that these assistants will get smart enough to understand context, eventually. Not to say that I don't get that such capability requires a significant amount of progress in the field of artificial intelligence. Nonetheless, I don't expect that we will have to wait for long.