---
tags:
  - perspective
image: ask
---

A couple of months ago, I was hanging out at one of my friends. I did not have an ongoing gig that day. In fact, I hadn't worked on any job that week. My eyes were glued to my computer and I was trying to fix a bug in my code. A friend of my friend, who has now become a friend caught me offguard with a question. "Hey man, I have never heard you mention that you are looking for a job. Do you even ever think about looking for one?" 

Honestly, I was lost for words, all I could do at the moment is laugh. I was laughing at my mindset as much as I was laughing at him for even considering it. When I thought about it, it is not about the question he asked, it is about the one that he really was trying to figure out. Sounds familiar?

> As always, it is not  the things people say or ask, it is about the ones they neither say nor ask.

 "How could you not look for a job?" To which I answered right after I was done laughing, "Why should I look for one?". 

Considering an 8am to 5pm job, there is no end to the downsides ... not to me. Unless I'm applying for a job that could give me at least a six figure salary from the onset. Fortunately for me, my age, academic qualifications and experience have me unfit for such a job. So my question, has always been, 'why take such a raw deal?' Why, when there are great alternatives.

Not to paint a bad image of these jobs. They come in handy, especially in the short term. It's not the jobs themselves that I hate that much. It's being denied both freedom and a handsome pay that I loathe so much. Plus the fact that you literally have to crawl to secure one these days; if you're lucky.

People are thirsty for answers, and  I have been asked  time and time again, 'hey man, how do I get to have an online gig like you?' Almost all of the time, the individual asking this question expects that I can get them some account in an online jobboard, or a flow of jobs from mine. This line of inquiry always strikes me as naive, and you can't really blame them. They have heard that online gigs pay handsomely, some of the times I can help, and at least they know they have to start somewhere. What many of them, don't seem to understand is that there's really no magic to the numbers they have heard about.

> Before I proceed, I must qualify that asking a naive question doesn't make anyone an idiot. Not asking enough times as is necessary does.

To earn these figures a significant amount of planning, effort and time have to be invested. Value is the only commodity that I have known to consistently sell. Which brings me to my point ... if you are to earn a sensible amount online, you have to cultivate a skill with which you can bring value onto the table. 

The other aspect that most folks tend to assume is the idea that only computing or writing services sell online. That couldn't be further from the truth. If you do an in depth research of all the services that there are across major online jobboards, the possibilities are staggering. Of course the other possibility that tend to be overlooked is that you don't have to work online. In every skill-primary market, it is the quality of you work that sells. In some fields, if you know your shit and you network okay, you will find it easy to secure jobs. Contracts are especially great.

That said, I do get the sense of where most people are coming from. They feel that they have offering these services isn't practical, and with good reason. Most of them wouldn't know where to start because they neither have the knowledge nor the skill requisite to deliver on such.

Fortunately, just because one does not have a skill does not mean that they cannot find ways to acquire it. In fact, educational platforms have been widely developed online, so much so that courses that would cost you thousands of dollars to acquire in a traditional college now cost you next to nothing. From my analysis, I'm compelled to conclude that some of this platforms offer better training than you would get in a college.

For those of you like me who would like to get their hands dirty with programming, there are dozens of online platforms where one can learn a programming skill. Some go so much as to offer a broad list courses in modern computing. For example, platforms such as pluralsight, lynda offer courses extensive courses in data analysis, systems security and ... well, check out the links towards the end of this post.

If one has made the choice to venture out to acquire a new skill in any area of computing that happens to intrigue them, there is literally a tonne of information out there. One only need do their research and prepare to adjust. Most things, if not everything in the industry changes very quick that it's one has no choice but keep up. While that may sound scarely at first, it's not really mental as it sounds. Once you get the gist of it, catching on new concepts is easy, that is if you like what you do. Chances are like you're gonna enjoy your new set of skills. That much I hope.

If you would like to try something different, there is no shortage of resources if you're eager enough. It is always better to look; you will be amazed by how much there is.

To identify which platform would be best for what you want to learn, it is easier when you do some 'serious googling'. Read through reviews, to see what is up to date, in depth, and right for your level. Similarly, almost all platforms offer some kind of trial. Make use of those, when you're exploring. Some are great, the worst are often average. I believe if the will is there, you will find what you are looking for.

I have done a couple of posts before that touch on [self education](/self-education), [mastery](/the-path-to-mastery), [dependency](/past-dependency), among others. This is my first post that tangibly offers a 'how'. 

Personally, I'm a web developer and I have studied almost everything I know using [TeamTreehouse](http://referrals.trhou.se/wdan). They have three plans. A **basic plan** that goes for $25/month, a **pro plan** that goes for $50/month and a Tech Degree programme that goes for $199/month. All the plans are jumpacked with great content. For those who would like to be programmers, you can get [a free trial and a 50% off your first month](http://referrals.trhou.se/wdan).

> All see an article I published on Medium titled  *[My journey to competence web development](https://medium.com/@onweru/my-journey-to-competence-in-web-development-da4d951bd98)*.

If you would like to try something different, see [Lynda](https://www.lynda.com/), [Coursera](https://www.coursera.org/), [Pluralsight](https://www.pluralsight.com/), [Udemy](https://www.udemy.com/). This list is just a beginning, there is so much more out there.

As the old adage goes

   > Seek and thou will find, knock and the door shalt be opened for thee

I say keep googling for answers. Scan through reviews and forums. Ask questions until you find answers, or you learn how to ask the right questions, then ask some more. It's the age of information, and empires of today are built on information. Do thou thirsteth for information?
