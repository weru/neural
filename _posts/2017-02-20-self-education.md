---
tags: 
  - perspective
image: read
---

I have often spoken in a tone that would lead some people to believe that I loathe education.
Reflecting on my conversations, it occurs to me that some people whom I have engaged might
mistake . I'm a big fan of education, but have an issue with academia.

Most people equate education to a bachelor, or master degree - and so they often stop at that. In a way, it used to be that with some effort, most people could land a job at this point. Few, however, can actually
can even realise their dreams by that now.

The argument against academia has gradually been gaining ground over the past two decades.
While the strongest critics of this system of learning are those who prioritise self-learning, the
anti argument resonates with everyone at some level.

Make no mistake; self-education and academia are not mutually exclusive. Used in tandem, they could prove to be
extremely powerful. While formal education defines the criterion by which knowledge is acquired, self-education leaves individuals to dictate their own standards for learning.

Even so, disregarding self-education is probably more dangerous than foregoing academia. The latter tends to condition people to memorize information for retrieval during tests. Millions of students around the world attend school to obtain certification, and thus the priority is often on the end result other than the learning process. This way, in most disciplines, it is easy for students to leave school almost the same way they came in.

Self Education requires deliberate effort by one who wishes to improve themselves in a particular area
or for the mere gratification of reading. It is for those individuals who yearn to broaden their perspective by opening themselves to a myriad of experiences. High levels of self-discipline are necessary for one to successfully self-educate.

Academia has made many of us <a href = 'https://www.scotthyoung.com/blog/2007/09/05/dont-confuse-a-degree-with-learning/' class = 'link-follow' target = '_blank'> confuse a college degree for learning</a>. It is one of the lies of our days. It is sad that most of us hold this limiting belief.

Compared to academia, self-education tests one's will to strive. Consider this statement from
Trump's inaugural address, *'we understand that a nation is only living as long as it is striving'*. It's a profound assertion. Nations are made up of individuals, and when a nation's citizens are striving, that nation is headed somewhere. The main problem today is that many people around the world look up to their governments to 'strive' for them.

For instance, consider these conversations

``` yaml
 Dave: These days it is hard to get a job with a bachelor's degree.
  
 Peter: True, one needs other accompanying documents and or a master's.
  
 Dave: I think the government should lower the retirement age ... old people should give young people an opportunity to work as well.

 Parent: This government should create good jobs for the youth. All my children have at least a bachelors degree and only two of them have decent jobs. The other three are stuck in shoddy jobs.

 Friend: I doubt that will happen, there is too much corruption and indifference among the political class.
```

Just to name a few, there a handful of other variants out there. As much as these conversations underline certain societal truths, they often miss the point, 'a government should create healthy frameworks for it, citizens, to operate in'. The rest is up to the citizens to figure out.

Self-education enables people to rise above [dependency mentality](/past-dependency/). It enables people to discover ways of tapping into their ideas and have the tenacity to lift their ventures from the ground.

