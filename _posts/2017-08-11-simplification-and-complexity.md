---
tags:
  - perspective
image: simple
---
I write for the sake of writing. Not only because I like the product after the last stroke, but also because I like to wallow in the process itself. The gratification that comes with the process better than the nature of the product itself. I write for no one else but myself; although I like to convince myself otherwise. 

That is why I seemingly write without purpose. And hop from thought to thought indiscriminately. Perhaps in an unguided manner. As though the stuff comes to me incoherently. Yet to me everything I note down is coherent. For I write as I think, as thoughts and ideas occur to me. If I could help it, it would be probably seem more coherent if I wrote as I would speak. However, that or the result thereof would be lame, and pretentious.

Which brings me to my point today - a point I have been dying to make. The reality that exists in our heads is too complex to be caged in one right specific manner, or in a set of patterns for that matter. It is an unbounded spectrum of waves which transcends the limited moment of time in which we experience them. 

For this reason, they may seem unorderly in the pretext of our finite simplifications. Not that simplifications are in and of themselves wrong. If anything, they help the world from going insane. It offers a manageable context to things, so that we might slowly grasp the basics that consititute our complex reality.

When you take a break from it all and really think about it all, keenly deliberate, it makes perfect sense then. The simplifications are not antagonistic of the complex stuff, for they are the very stuff that the complex stuff are made of. Therefore, I have always thought that stuff should be considered as worth of being the way it is.

When I consider things, I tend to go over as many possibilities and details related to them as I probably could. In my head all these concerns occur concurrently or on a super rapid context-switching basis. Putting it differently, it may as well be the case that it would appear that they occur randomly - thus the erroneous presumption that they are incoherent.

Wait though, can they really be truly incoherent if they are related? May be, may be not. May be, because a reader who doesn't see the entire context might be lost while they are at it. It would then be said that it is somewhat my duty to make sure they can follow my internal discourse with the least help. The slippery slope, however, would be that it's almost impossible that *they all* will follow.

That said, it is not a tiny bit incoherent to the extent that to the right audience, the dots are all there. The right audience have a good time connecting them alright. I, for one, I'm my own audience and it all lines up fine - that's why I write for myself first and foremost. Narcissism? Someone said that the best artists produce the best of their works for themselves; and choose to share afterwards.

To this point, this post has laid enough groundwork to now underline its message. The known universe and the beings within are incredibly complex. So much so that the most basic parts of our civilization are authored in a way that seeks to mask the complexity with simplifications. For example, language and definitions are simpler and repetitive ways of describing people, situations, experiences and phenomena. It is great because it helps the society to function smoothly, perhaps in a synchronized manner. 

That is a good thing, but it does not prohibit us from trying to unmask the complexity that we are part of. Not everyone has the knack for what it takes and for that reason, the unmasking process seems chaotic and incoherent. While taking apart the individual components of the whole is a tedious job already, identifying how the parts are related is even harder.

While attention doesn't come easy, this task asks for every drop of it. Intellect and the ability to observe is of utmost importance. Only by being objectively observant and analytical can one be able to see clearly and more intimately. Otherwise, we are individuals who cannot see the forest for the trees because we are unable to see the simpler parts, as they relate to the whole. 

This line of thought would then beg the question, *is everything complex worth simplifying?*. To that I would not utter a straight answer. What I can say, however, is that not everything simplified ought to stay simplified.
