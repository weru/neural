---
tags: 
  - perspective
image: artist
---
Sticking to an endeavour has always, is and probably will always be the mark of individuals who exude toughness.
While biology, environment, wealth and status have often factored in how well one might weather the journey, the greatest
determining factor has always been mental tenacity. Without it, even with the aforementioned factors at ones's disposal, one 
can only go so far. At some point, tragedy strikes in a way that renders these factors blunt in the face of it.

One could argue that the world and the societies we find ourselves in have evolved into a complex system that makes pursuing certain goals a daunting task. This is a truth that only a deluded individual would try to refute. For  example, the world's population has continually ballooned into almost unsustainable levels. Resources, on the other hand, have increased at a rate that barely keeps up with the rising demand. This creates a bubble that gets people anxious. People wonder, 'What if it's about to burst'?

While this is just an illustration of a general observation that requires no special analytical ability to identify, it helps 
depict the anxiety a young man, in 2017, may themselves in. Many people will recognize an internal monologue that I'm about to illustrate. It is always in the form of
 
>I would like to 
   * (1) *do a number of a,b,c,...* 
   * (2) *achieve ...,x,y,z by the time I'm, say 40.*

Deductively, to do so I would need
  
 > * Need financial capital
  * In the amounts of say 100 dollars
  * In this number of  days or weeks, for t duration of time.

*Unfortunately, I don't have that amount of money or don't see a way of generating it by then.*

By now you should complete the monologue on your own. I bet you will agree that this monologue can go on for days, decades or durations in between. Sometimes, that inner conversation dies as quickly as it starts - that is probably why we are encouraged to write down our ideas somewhere. A friend of mine told me once that one should have a notebook and pen by their bed to take notes of ideas before they sleep. Of course I made fun of the idea because I thought it was crazy, and it is, but all the same true.

However, like many things in life, if not all, there is another side to this argument. While things have changed to make pursuing a goal difficult, a myriad of other things have cropped up to make it possible to pursue  a tonne of goals. Stuff that would have been unfathomable a decades or a century before. For instance, while natural resources are fixed, humanity has become much better at identifying, utilizing and recycling previously unidentified resources.

*In a way, it could be demonstrated that it's not scarcity that holds humanity back, but rather the scarcity mentality and the
illusion of limitations.* It's no wonder the richest countries in the world are not necessarily those with the most fertile soils, minerals, oil reserves, or most favourable climates. Not that this very conditions haven't propelled some countries into economic prosperity, but they are hardly the differentiating factor between the developed world and the third world.

Scarcity mentality and exaggerated limits hold back individuals of all nations - well, some more than others. Wherever such
perspectives are silently or publicly entertained, greatness, prosperity and actualization are forfeited.

I could keep exploring the subjects of anxiety, scarcity and invisible limits. Touching on them, however, was necessary to demonstrate why one who intuits that they have a worthy endeavour should not recant their resolve to find ways to pursue it. I will settle for mastery henceforth on this posting.

To be successful, one needs not be a true master of what they do. However, it's improbable that a master in a given subject will ever be unsuccessful at it. Mastery is the level at which a certain practice becomes an the extension of an individual. Like breathing, even the toughest things to perform are done with effortless ease and grace. True masters often exude some sort of halo effect whenever they do what they do. 

Not only do they err less, but they also come out as not trying too hard to get things done. In my opinion, seeking mastery is the ultimate path towards self actualization and happiness.

### **Single out a goal**
The first step to mastery is identifying an idea, practice or goal that one would like to major in. Not the only 
thing that one does, but the only thing that they focus most of their energy, attention and time to accomplishing. It's the stuff that they are consumed by, enough to let themselves be absorbed on all levels by it. It could be a known area, under-explored, or a completely strange idea. 

It's a personal goal that nobody can give another. They certainly could point you in its direction but they cannot choose it for you. Once you identify what you want to focus on, you'll be well beyond the starting point. Better yet, it will give you an idea of the direction you must follow. Simply, it becomes easy to plan going forward. From there, a wise man goes forth to
lay the foundation.

### **The basics are indispensable**
Achieving mastery is a journey too long to skip the basics. As a developer, I thought I could beat the system by lightly brushing over the basics. This mistake cost me over an year of trial and errors - with an absolute mess to show for it. I made the mistake of under estimating the basics and trading the time necessary to master the basics, all in an attempt grasp the advanced stuff - **'the stuff that pays.'** Did they pay? Your guess is as good as mine - they didn't. Not without the basics.

Today, I know better. I would rather spend months mastering the basics than blindly rush on to the *'stuff that pays'*. The basics are the truly **the good stuff - the one that actually pays**. They are the most essential tools of any trade. A person who tirelessly practices the basics until they can perform them as well as they breathe, will master an area long before those who treat the basics as fluff.

Although most things are often worth more than the some of their parts, the individual parts are often irreplaceable
where it matters. The basics never disappoint; haste does.

### **Obstacles are part of the hustle**

After the basics are mastered, the foundation is set but the building must continue or the basics will be useless by themselves. While the basics are often too easy or monotonous to the extent that they elicit boredom in many people, they are the easiest to master, rapidly. The other stuff is always deeper and require hundreds of hours to master. Surviving this phase tests one's mettle. What are you made of?

The list of obstacles are endless and often exogenous. All the same, the greatest obstacle to progress has always be the individual trying to learn something. While capital, time, environment among other nuances are impossible to wield absolute control over, it's often within our grasp to control ourselves. That said, it is often easily said than done.

Since self is the only variable that we can have direct control, mastering ourselves should be an important part of the path to mastery. Unless self discipline is attained and practiced, mastery over anything else will elude us for good.

### **There is no end to learning**
Masters are never islands. They understand the value of instruction. They are individuals with a fair grasp of things they
do know and things that are unknown to them (wisdom). This knowledge is a consequence of knowing who they are and what they're not. The reverse could hold true as well.

To avoid stagnation or falling off the path, masters study from the field of knowledge already available. While they could be
creators of knowledge, techniques and experiences, they value the experiences of those who came before them. As a consequence
of that wisdom, *they sign up for classes, buy books, hire personal trainers, join communities ... whatever they have to do.*
They feed on knowledge.

This knowledge then helps them build their level of skill and their mental tenacity. Most importantly, it helps them
scale up their level of mastery. Even in periods of perceived stagnation, they will be improving. It doesn't even show on the surface. No wonder the shallow repeatedly miss the point of practice.

Robert Greene writes in his book, **Mastery**
*"At the heart of it, mastery is practice. Mastery is staying on the path."*
*He also quotes a Japanese sword master*

  > *"Do not think that this is all there is,*
       
  > *More and more wonderful teachings exist,*
       
  > *The sword is unfathomable."*

### **Who should pursue mastery and why?**
Ideally, everyone should seek to be a master in a given field. However, very few people can achieve mastery. It just the way it is. Crowds lack the juice necessary to achieve mastery. It's for the mentally tough - the good news is that mettle can be cultivated. 

It's in the trying that one can really learn to swim. Although one might not improve enough to win a medal, and most often 
achieving a medal is never really the point. In the same way, even if one does not achieve mastery, they will certainly achieve some degree of expertise in their endeavour to be successful.

Achieving mastery will automate success in any given field. It will be rewarding in and of itself even where financial success is not a big component of the success. After all, most people are not unhappy because they have less finances than they would like to have. Their root cause of unhappiness is wasted time, opportunities, talents and failure to self-actualize. Don't get me wrong, money is always a crucial component of a happy life. It's just that money chases after masters anyway.

Whom then is the guy that can overcome challenges and tragedy? Who can tame and govern self into going for what they truly yearn for? Among such are the ones who given time, will achieve mastery - their legacy.

May we all find our paths to mastery. May we weather the process; all the best on achieving mastery.
