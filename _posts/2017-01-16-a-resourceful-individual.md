---
tags: 
  - perspective
image: resourceful
---
Resourceful people are far and in between these days. Everywhere you go you find individuals
who want to take away something with the least amount of input or none at all. Entitlement is really the
plague of our days. 'What can you do for me' mentality has left many of us dependent, delusional, deceitful and untrustworthy.

Humanity requires of us to seek resources to sustain life - the challenge has always been to nourish and propagate life.
As such, seeking to obtain resources is in and itself a necessity. It is therefore in the society interest for its members to seek
out resources. The rules of nature, however, require that one who takes resources to contribute something equal or perhaps greater
in return. In some circles this would be referred to as sustainable development.

What then constitutes resourcefulness? What must an individual do to achieve such an ideal? Following our earlier discourse, one
only needs to earn what they get from their environment, not because of their mere existence or greed but from their active input.
Reciprocity is a noble principle to live by.

I would like to qualify that being resourceful does not in any way equal to martyrdom. On the contrary, it comes from an elevated
understanding of one's personal needs and wants and how one can satisfy those needs in a way that doesn't rob his environment. It 
means being able to pin point what other individuals need and want in relation to personal interests and highlighting the links to
those interests. Knowing what the planet needs, and tuning your skills, talent, and hobbies to enter into creating a mutually
beneficial arrangement.

While martyrdom milks an individual's effort often at their own expense, resourcefulness enables the possible greatest good for the
greater number of individuals. This means, if one is an entrepreneur, their services will better their clients lives 
while at the same time improving their financial base. It is enlightened self-interest.  

# Resourceful individuals make great friends

Resourcefulness dictates that an individual will always find a way to contribute either by action or through proxy. They criticize
to instruct and to correct. They regard efficiency and logic over sensibilities - they won't allow themselves or those around them
to be fickle. That is why they're the best friends one could have. Their passion, insight, personal integrity and 'nononsense' 
attitude help them build those who appreciate their intentions.

Unlike 'friends' who will give you their shoulder, only to encourage you continue wallowing in self-pity and inaction, a resourceful
friend will call you out on your weaknesses. They understand that although you are a sum of your strengths and weaknesses, it is the 
latter that eclipses the power of the former. Consider the following equations.

### Average You = Your Weaknesses + Your Strengths

### Exceptional You = minimize(Your Weaknesses) + maximize(Your Strengths)

Nothing you haven't really come across before, but still worth be reminded over and over until you take it personal enough to act.
