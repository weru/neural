---
tags:
  - tech
image: dev 
keywords:
  - web development
  - teamtreehouse
---

Three years ago, I was well on my way into attaining my Bachelor’s; I had one year left. I figured pursuing a master’s degree didn’t seem like a smart choice. Fortunately for me, I was drawn towards working with computers. 

I had a friend do a few things using code that I thought were neat. I’m an inquisitive individual with nerd tendencies so programming sounded just about right for me. I knew I wasn’t going to sign up for a computer science course in some college. Mind you, after high school, formal schooling drastically lost it appeal to me, or maybe college ruined it for me.

The biggest challenge for me, which I did not know at the time, was that learning to program by myself would be challenging. It is hard not because there are few resources. It is hard because there are far too many resources online. This makes it difficult to determine what is important and what is not. Two, programming is a vast industry. Thus, it is necessary to choose a specific niche and stick to it until you’re comfortable to venture out.

I walked through lots of resources on **YouTube, w3schools, and TheNewBoston**. While these were good resources to get a sneak view of what programming is about, there was simply no structure. No direction. I studied *java, html, css, python, html, javascript, jquery*. If you have tried learning anything on YouTube, you can guess how it went down.

Two things happened. My learning became intermittent. Secondly, I thought I had to learn everything to become a programmer. As a result, I remained in the amateur zone, very incompetent and with no meaningful projects to showcase. Worst of all, I regret the time I lost. Two years were wasted on silly back and forth; confused. Talk of stagnation, unless I changed the way I was going about it, progress would be impossible.

There is a thing about having an inquisitive attitude. With it, the odds of failure are abysmal. The resolve to get better answers pulled me out of stagnation hell. Being an avid reader, going through dozens of blogs on the subject brought me to [Codecademy](https://codecademy.com), then to [CodeSchool](https://codeschool.com). All the while, I was very evasive of any plans that would involve me paying up. So, I stuck to plans that were entirely free. While that is ok when you’re starting, freebies can only get you so far. The plateau is always right around the corner; sooner than you might think.

Those days, Codecademy used to be completely free, and their curriculum was good. That said, there were no real projects, no paths and I actually struggled to transfer the knowledge I learned into a real-world project. All I could do is feel good and awful at the same time. Good because of all the badges and points I had accumulated. Awful because those badges, points and knowledge did not translate into much once I opened an editor to start a project. I couldn’t figure out how to move to point B now that I had managed to accumulate that knowledge … the dots were all over the place. In all fairness to Codecademy though, their courses have dramatically improved over 2016 and 2017.

Looking further, I came across [Teamtreehouse](http://referrals.trhou.se/wdan). There was something about their reviews; I just had to give it a try. After all, they had [a 14-day trial](http://referrals.trhou.se/wdan) which I could cancel at any time. After the trial, I figured I could cover a good proportion of the content I need in a month or two. I now know I was wrong about that.

When I started learning with [Teamtreehouse](http://referrals.trhou.se/wdan), a whole new world opened to me. Finally, I could get to point B. Their content was not only outstanding in terms of quality, there was a sense of organization to it that was neat.

TeamTreehouse has tracks that pack subjects specific to a given niche bundled together. For example, there are different tracks for IOS development, web design, android development and about a dozen of other tracks. This feature is very important because it enables you to develop your skills progressively in one area, not only saving your time but also increasing the odds that you will devote your focus on a specific craft. For me, it was front end web development.

Unfortunately, my mindset was wrong. I stuck to my plan to study for only two months and then paused my account. *Good resources beget progress and that progress begets arrogance*. Although still an amateur, I thought with my newly acquired skills I could soldier on without my subscription. In a way, I thought the knowledge I had gained to that point was somewhat ‘sufficient’. Boy, and to think of how wrong I was! That said, my assessment wasn’t entirely wrong because for the first time, I ventured out, built and deployed a website on my own. In fact, I built over half a dozen sites while my subscription was on pause.

Over time I have noticed several things though, things are constantly evolving. Two, there are a myriad of ways of approaching development tasks. While all approaches are fine to the extent that they work, some of them are simpler, more efficient, and elegant. Others are hacky, less efficient and definitely less elegant. In short, learning the most up to date and efficient approaches in our craft can result in less time wasted, cleaner code, actual mastery and confidence in our craft.

> Robert Greene in his book __Mastery__ talks of the unrivaled benefits of studying under an instructor as opposed to hacking our own ways. For me, that fact has been personally observed through trials and failure.

My advice to individuals eyeing a career in development: It is best to signup for an account on a competent platform where they can learn under experts. If the platform offers opportunities to develop real word projects, so much the better. There are a couple of solid platforms such as **[Codeschool](https://codeschool.com), [Lynda](https://lynda.com), [Pluralsight](https://pluralsight.com), [TeamTreehouse](http://referrals.trhou.se/wdan) and [Udemy](https://udemy.com)**. Of course, there are others out there, but I feel that too much options results in a paradox of choice. So I will limit myself to listing just a handful … the best I know.

At the time of writing this article, I would argue that TeamTreehouse is the best platform to learn web development. Among the best things I would laud the platform for include:

### 1. It has an excellent library of courses and workshops

Other than the excellent prepackaging of the courses you need to master a specific skill, these guys go an extra mile in keeping you up-to-date. Their courses are constantly being updated. They add videos on top of the existing ones, but they will also once in a while do a refresh of courses. Therefore, you have the confidence that you’re learning the most recent stuff.

> It is also the kind of feature that fosters *lifelong learning*.

Their content is not only to the point, but is also thoughtful enough that you actually learn. This balance, ensures that there’s neither fluff to go through, nor is the content lacking in substance. If you have watched videos on YouTube or on other platforms and thought the content felt rushed, lacking in substance or both, [TeamTreehouse](http://referrals.trhou.se/wdan) is definitely for you.

### 2. The community is excellent

Even with great content and excellent teachers, getting stuck while coding is guaranteed. Especially when you’re starting out you will probably get stuck on the quizzes and challenges. This is not to say that only beginners find themselves in such situations. At my level of proficiency, the likelihood that I will need someone to help me out with my code never really fades.

While robust communities such as Stackoverflow come in handy when you hit a bug, it’s not uncommon for new comers to meet a rude shock when their questions are not novel enough. I find that they are better suited for seasoned programmers who have the gist of how and when its appropriate to ask for help.

TeamTreehouse’s community is vibrant and accommodating enough for all your questions. Questions are asked every other minute. From what I have seen, only questions bordering on being rhetorical go unanswered.

In my experience, answering questions is incredibly rewarding. I get to flex my knowledge on what I have learnt. Sometimes, I have to test my own solution to ensure that it works. Often, you will come across challenging questions that will prompt you to do some research on something you hadn’t thought of. My best experiences are when some other student comes up with more elegant solutions. It’s a learning experience.

### 3. Discounts and student perks.

I deliberately saved this part for last. If you’re convinced that Teamtreehouse is a probable pick for you, well, this is for you.

Like I mentioned earlier, they offer a free trial. You could also [get 50% off your first month anytime](http://referrals.trhou.se/wdan).

Once you’re signed up, they offers you discounts for every student you refer. Trust me, if you stick around long enough to consume their content, you will want to refer someone else. Their content is just that good.

Finally, they offer other student discounts, called student perks. Here they offer you access to other external libraries and tools that will aid you in your career. Some are completely free, others are at a discount.

> In conclusion, the story of Teamtreehouse is one that can be summed up in one word, VALUE. Value for your cash, time and effort. If you make the choice to make Teamtreehouse your learning platform of choice to learn programming, Happy Coding!