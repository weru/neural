---
tags: 
  - perspective
image: time
---

Humans beings are notorious for underestimating the intangible. Time, space, gravity, air, breath, balance, eyesight. The list is
quite handful, but this post will dwell on time alone. What is time? How does it mean to different people? Can anything be added or 
taken from it - can it be manipulated?

Science fiction suggests that time could be manipulated - maybe we could time travel in future. Oh boy, who wouldn't like to travel
back in the past or forward into the future? Anyone? I didn't think so. I'm not one to dispute scientific 
theories. History has proved that there is so much about the world that we did and do not now. However, up until such a time when
our technology can help us augment time and space, the value of our lives will always be tied to the *limits of time*. For everything and everyone you know or you're unaware of exist in time.

Wait, but is time that has limits or is it our biology that is limiting? It would appear as though the latter is more plausible. Even so, our concern is that everything thus is done or that happens due to naturalistic causes occurs within the confines of time. As such, absent of time, it would be improbable for anything that happens to happen - absent time, there's no existing.

It would therefore be wise to conclude that we owe everything that we do or experience to time. Yet, most people due to lack of energy, ambition, discipline, self-worth or a subset of these reasons perpetually continue to waste time. But then again it is not surprising because it's hard to value on something that one is unaware of.

**Time exists as a continuum.** We're born into time and grow up, and die in it. Whether we are consciously aware of it or not, we decide how much time is worth to us. For the truly ambitious, they make the most of it, while the lazy and shallow squander theirs as though they have an eternity to live. By focusing on being productive, an ambitious man.  

Most people are happy to just to exist and, therefore, have no problem renting out their time for peanuts while other could care less about wasting it. The general attitude is often like, 'whatever gets me through today, this week, this year'. Man is often either living in the past, reminiscing about past victories, or in the future fantasizing about how good the future will be. The present is brushed off as uninteresting, troublesome and sometimes boring.

The past is done, and the only way to improve the odds of the future being greener is by being productive in the present or buy actively investing in self. Since we will carry ourselves into the future, the investment will be tapped in the future. The bottom line however, is that present action is necessary. To act, distractions about the past, present and future have to be brought to the minimum. Distractions will always be around, hence we have to cultivate personal mastery. 

When the sacredness of time hits us, a couple of things about whom we are as individuals and our lifestyles take a turn:



**'Know thyself'** is the mantra of those who seek discipline. Discipline helps us to value time, to make the most out of it.
