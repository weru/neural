---
tags: 
  - perspective
image: pain
---
An average man is a losing man. He is used to pain and suffering to the extent that he doesn't dare to see past his pain. 
You maybe thinking that maybe he enjoys suffering. That couldn't be further from the truth. He like anyone else hates suffering; he is terrified at the mention or thought of pain. He, however, feels helplesss  - and he confuses those feelings for fine logic. I'm willing to bet that he's a 'feelings' man - and mood-centric loser.

So one might ask, is he then a man with lesser intellect, low IQ perhaps? Not necessarily, he could do amazing things with his
intellect. His brains are very capable, but all the same under utilised. 

By default, he is a lazy man. One who wishes that all things came smoothly and easy. He is always complaining that the world
is unfair and that life sucks. It is as though everything and everyone around him are always conspiring against him and his 
interests. He not only refuses to see the errors of his faults, inadequancies and inaction but expects others to laud him for what
he's not. He places his point of locus on the external.

He is a whining loser. How could he not be? While he professes his love for progress and diversity, he is anything but in his actions. His words lack congruency with his life choices. Should he starts a project, you can always bet that he will quit somewhere along the middle. When he quits, he starts something else in the name of diversity. Funny how the term 'diversity'
has become the rallying cry of under-achievers - *we know our products ain't that cool, but we have a myriad of choices for you
 to choose from.* Please, most folks couldn't care less about your *'tonnes of choices'* as much as they'ld appreciate __*great stuff*__. 

He is a man with no backbone. He brings everyone else down with him - the very people that he often blame shifts his failures
to. It's not only sad that people who thought he could show them the way become his very victims, but also that he has the nerve to look at treat them with contempt - as though they are his accomplices.

A losing man either fails to truly grasp the depth of his potential, or is too fickle to do something worthwhile about it anyway. In every respect, he is an idiot that neither deserves anyones mercy nor attention.

### **Tells of a losing man**
#### **He consumes more than he produces**

Because he is a fun loving man. He really thinks that he could get by having the *'fun life'* just like the people sees on tv. I'm assuming that you probably already guessed that he is a man who loves the movies. His entire life is, therefore, framed in
the context of what his sees on the movies, and on social media - he is somewhat 'social' too. It, thusly, doesn't come as a 
surprise that he yearns to live up to the hype.

He showers himself in his less that proportionate silver. He borrows from his relatives, friends and banks to finance his make believe life. Not that 'faking it' is entirely a bad concept, but he naively stretches its meaning - imagination gone amok. He is a man who repeatedly misses the wisdom of leaving within his own means.

#### **He is responsibility phobic**
 
This kind a guy lacks backbone. If only he could be assigned the least amount of tasks, that require the least amount of effort to perform. If only the world was a fair place, where smooth means to implement his agenda effortlessly were the rule and not the exception. He likes to be supported and guided into action. He is a man who loves the success of outcome but loathes the effort required to get there. 

#### **He is a sensitive moron**

A man who abhors responsibility is likely also one who hates to be reproached for his inadequancies. He is not only a man without a backbone but also one with a distorted sense of self. He will often rationalise that everything about himself is ok, or above great - and he will blame everything and everyone else for what he is not. An attack of his inadequencies bruises his inflated ego. Normally, he will default to thinking - no, feeling - that he's being insulted or treated unfairly.

Unfortunately, such an individual has his ego so tightly coupled with his sense of self that anything that rubs his ego wrongly is equated to an assult to whom he is. It's ironic that a man who doesn't know *who he is* to think that *who he is* is being assaulted. Whenever this happens, and it happens oftens, he lushes out at the one is pointing at his flaws instead of taking heed and self correcting.

Since he lets his sensivity get the better of him, he throws the capacity or willingness to process logic, information, cause, effect and remedies out of the window. 

The worst thing about this man is that he is too fickle to even at least self analyze, criticise and self correct himself. He is a man on an infinite loop of deeply flawed rationalisations. His life becomes a battle even to himself, because he is too sensitive to be flexible or to allow growth. He senses that other people can see that something is off with him but he is too proud to admit it even to himself. Such a man is a moron that cannot be helped until they embrace the power of reason and see the 

#### **He prides himself in being okay**

Wielding a mind that gets off on external validation, this man dares to go as far as is considered 'cool'. He may still talk of his dreams, but they convincingly continue to sound like distant wishes. 

A losing man is one that lives his life through the filters of the society. He clads in the society's garbage and accepts it as his own - always seeking to please while avoiding reproach at all costs. He's a man who has given up on himself, a compromising individual.

Is there redemption for this man? Of course, but only if he seeks it for himself. If he is to awake, he needs to have a personal awakening, either prompted by tragedy or by an epiphany that he is his own victim. The villain that he seeks is himself.

I have been that man, probably you too have been there, or are there. Can I help you? The answer is no - I have miles of my own to cover; I'm busy working to be a man that wins - a thriving man; an eagle. Criticize me, I will take it personal enough to self correct. Until mastery is achieved the mantra is **rinse and repeat**.
