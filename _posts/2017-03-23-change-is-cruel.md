---
tags:
  - perspective
image : waves
---
I once read that change is a muddy process. The depth of these words did not sink in well until in the last couple of days.
It's often said that we should strive to learn from other people's experiences - or else we are fools. There is a caveat to 
this observation though. Even when we observe others and promise ourselves to conduct ourselves in better ways, things happen
very fast. So fast that we’re unable to discern the similarities in our own experience and the claims made in our vows. At
least not until it's too late to change course - not that we would be inclined anyway.

We, therefore, cannot always benefit from the experiences of others. What we can do, is make the most of them. We're not 
all-knowing, ever-present. The curse of our limitations is err.

The events leading to change constitute a cruel process and the ugliness of it might overwhelm the yet-subtle beauty of its
product. Change is the birth of a new creation that is by all means bloody at its dawn. The very nature of it instills fear,
doubts, regrets and in some instances shame. Yet in the end, all these cons fade away in comparison to what comes of it. It's
the cost of change - it is the way it is.

In this post, I will ponder over behavioural changes. Changes that have a bearing on someone's personality. Often these changes
occur following an epiphany that some aspects about self are subpar. Consequently, as it is in our nature to strive towards
personal improvement, we decide to do something about it - to reorganize. Here comes the point where we start to rock the boat. We start agitating homeostasis - the mechanism that keeps things in normalcy.

Homeostasis has balls, and it sure as hell doesn't just fall in line. It defiantly stands its ground. It glares in our face 
and sneers, “If you wanna stir up things, you have to go through me”. This is where chaos start. The truth is that no matter
how much we think we have everything at our fingertips, we really don't. Mr Robot, in the 2016 TV series *‘Mr Robot’*, calls it the **‘illusion of control’**. That's right, the feeling of control that accompanies resolutions is often just an illusion. 
Irrespective of our research or ‘due diligence’ measures, we have only figured out one dimension of the change process. 
Unfortunately, we live in a multidimensional universe. 

As our illusion of control meets homeostasis; homeostasis ruthlessly kicks ass. For a moment we are lost in time, just 
floating in space. All we can see is a gazillion shattered stars. Even the bystanders are struck with disbelief, *ouch!* All we can hear is a fuzzy distant sound of our past form 
rebuking us for daring to break up with it. We panic; our worst mistake. Damn, homeostasis is the ultimate heavyweight with 
the punch of a forklift, yet. BOOM! BOOM! BOOM! … friends, it doesn't stop. There's no referee and the rules are decided by the 
opponent. We’re in hostile territory and it's all our fault. We did not get an invitation - we blindly sneaked in as if we 
owned the place. You wanna own the place? You’ve gonna earn it fam; by surviving and kicking ass.

Focus is all we have to master. Every punch that lands on us have to mean something. Adaptation is the goal here. If we can 
reorganise fast enough, homeostasis can be forced to a corner. No kidding, there is no crushing homeostasis but we can
negotiate a truce. May we live to tell of it.

Like all battles, this one is no exception. In fact, there is more than proportionate collateral damage. People closest to us
fall as victims. They're killed or maimed beyond hope of recovery. If only we knew the price could be this costly. We feel 
responsible - guilty, in fact. In our eyes, the blame weighs heavily on our shoulders. We blame ourselves more than we
do the opponent who actually moored down our friends. If only we conducted ourselves better - as if we knew how to before.

We are somewhat blind to our form. Since we have renounced our old, mature form we are now toddlers. We must learn to 
familiarize ourselves with our newly acquired reality. For that we will need more help than we are willing to admit. Even then
we will grossly make a fool of ourselves. Unfortunately, this time we don't have the luxury of physical toddlers. We cannot
quite get away with our slip ups like they can. We will be held to the standards of adults - as we physically are. It's not 
our privilege to put forward excuses. It's a lonely world (*'a man should reserve his feelings for his dog and beer'*, right?), but hey! There is some hope if we see it through.

The injuries we have incurred, the pain we have suffered and more importantly the friends we have lost have to mean something.
The sacrifices we’ve made will never truly be forgotten. In time we will learn to compartmentalize the pain of loss and 
prepare ourselves for future battles. It's a rough process and the big bang is an ongoing process.

Now that we know we cannot make excuses, should we apologize? Perhaps we should, but there is a good chance our verbal 
apologies will never make the cut. The damage is done. Secondly, toddlers don't apologize for their weaknesses. As much as their
weaknesses might be inconveniencing, it is not really their fault. But often those toddlers grow into teenagers 
and young adults - and at this point it is their fault. Ignorance is no defense.

Seeking forgiveness is a selfish act in and of itself. It asks the offended to overlook mild to atrocious offenses, with 
nothing in return except the promise of cease and desist in the future . But we’re selfish, so then must we apologize? If we 
must, words hold the least weight, although they offer a considerable buffer. The ultimate way to apologize is to conduct 
ourselves better next time we find ourselves on the field of battle. Not perfectly, but better. This we must and will.
