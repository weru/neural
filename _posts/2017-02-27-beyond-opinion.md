---
tags: 
  - perspective
image: brain
---
To lead an exceptional life, one needs to break away from what is common. To try out the unbeaten path will test your resolve in more ways that you could
never have thought of - it is just that involving. The greatest opponent is the old self. You are essentially in an unknown
territory, that bears a resemblance of familiarity. The truth is that when it comes to ourselves, our knowledge is often wanting.

How can one change their circumstances if they comprehend so little about their environment and themselves? Gradually is the word.
Step after step, until miles are covered. There has to be an understanding that things just don't happen, they are nurtured through
effort and focus. Time is a crucial element in this reality and so is patience.

*Distinguishing fact from opinion* is a skill that those who want to improve should cultivate. Facts give us a clear picture of 
how things are. From that point, it is possible to make pragmatic choices. 

Take a gym setting, for example. Mediocrity is not entertained. The environment inside the gym is characterized by people busting
with energy - the positive kind. You meet people who can see a pumped rookie in you while in fact you are anything but, yet. 
They have faith, because they were once beginners like you and worked their asses to greatness. They understand the marvel of focus,
the value of training. 

Through conversations you learn the importance of things that you once were ignorant of. The value of a quality diet verses a giant 
morsel of fluff. To paint a mental picture of what I'm getting at, **it is a balancing act**. There are weights, programs, reps,
sets and diet. Don't get me wrong, there are a list of other nuances that are involved.

To get the balance right, one has to train among peers, superiors and preferably under a trainer. Getting results is not easy
as snapping one fingers. There is a lot to be learned, resistance and self-doubt to overcome. From my short experience, it is 
very easy to fade back to inaction. 

If you are sharp, you will realise that it is easy to get fall into bad habits. Not all people in the gym know what they are doing.
Some have in fact adapted routines that hurt their bodies and or their progress. The sad thing is that most of them are rather opinionated and like
to share. Hey, you're here to build not stagnate nor do you wish to waste your valuable time for nothing. Listen, but be careful
to take everything with a grain of salt. Not everyone who sounds smart knows shit.

People will tell you that everyone is entitled to their opinion. That you should give everyone a fair shot to be heard. However, most 
people are ignorant - should they be entitled to be ignorant? I guess they should only if they can back those opinions with logic
and facts. Most people being intellectually lazy, tend to be notoriously subjective in their arguments, I have decided to always 
dig into any reading material I can get my hands on. 

You make the decision to be informed. For purely selfish interests, you would rather be aware of the
most optimal ways of getting to where you would like to go. That means that you have to vet even your own opinions and adjust accordingly
where they are subpar.

Into the second week, and that tiny voice is still there whispering, 'You can stop now, you dont have to'. You have become stronger
though and for now you know you have to. You will eat more regularly, train and keep reading.  
