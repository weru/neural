---
image: frustrated
tags:
  - tech
keywords:
  - development
  - developer
  - niche
  - self-taught
---

> The best developers in the world are self-taught, self-driven, and industrious individuals. It's because of these values that they are able to get ahead in their careers.

Different individuals choose this career path because of different reasons. Among the key reasons, I would say, are passion, the flair associated with the industry, and the earning potential. I would imagine that passion as a motivation increases the chances that one will in fact make a great developer.

Down the road to becoming a developer, there are lots of individuals who in spite of their initial interest, wind up giving up. Some realize faster than others that they do not quite have what it takes to acquire the needed momentum. Sometimes, this realization is born of careful and deliberate evaluation. Sometimes, it's a consequence of one or a series of miscalculations and disillusionment.

![tired dude](/assets/posts/tired.svg)

I have a friend who has been setting up their "development environment" for well over 2 years now. We joke about it from time to time. It's funny how much time he has taken fussing over trivialities. He's says he want to be able to develop from linux and windows environments. To be a proper developer, he also has the notion that he needs to study python, after which he will learn javascript, css, sass, a JavaScript framework or two. These are just but a few of the things that he has laid out in front of him as requirements without which he cannot become a proper developer. In fact, it appears to me that this list continues to balloon with time.

Of course, in my friend's case, it is my believe that pure ignorance is not the only issue at play. There's also a problem of mindset. I like to call it the **"curse of perfection"**. Eyeing a list lofty goals that must be attained before progress can even be conceivable. This kind of mentality coupled with ignorance, if prolonged eventually leads to frustration and self-loathing such that inevitably the dream of becoming a developer ultimately dies. After all, isn't it true that **what doesn't grow decays**?

I have been lucky to work on projects with clients that are themselves upcoming developers. From some of these experiences, it has become clear to me that the need to achieve perfection is not unique to my friend. For what it's worth, I have no problem with anyone seeking the highest standards for the products we develop. If anything, competence is one of the true indicators of a great developer. That said, this quest, particularly in tooling, swings too far that productivity and progress are sacrificed.

Aspiring, new, and seasoned developers need to be constantly reminded that the industry is not only vast but that it's also always rapidly changing. This way, to anyone who doesn't have a solid footing on any given niche inside the industry, the chances that they will find the environment chaotic is very high.
 
> *It's is also rather probable that they will easily be slapped back and forth by all sorts of fancy technologies, frameworks, patterns and predictions.*

This madness continues until such a time that they are left disillusioned. I surely would hate to be that kind of a guy. 

There is a thing that irks me about developers wanting to everything to be perfect. I guess, part of the reason why it does is because I was once stuck in the petty conundrum of my own making.

I understand that seeking perfection is one of the core principles that helps developers achieve better results every time they build new things. That said, more often, this is their Achilles heel.

One has to ask themselves, "At what time does my need to have perfect control actually starts working against my goals?"

Why would you spend a lot of time setting up a development environment? Why would you fuss to much about using particular tools, while there are better and simpler alternatives? What are you trying to prove to yourself? Isn't it better to not working against the current?

### Start Small, and consistently add to your skills set

To anyone out there who wishes to be a developer, I would say, *it is never that serious*. You do not need to learn half a dozen languages, half a dozen frameworks, tools and principles to get started. While starting out as a developer, the phrase "less is more" will not only always suffice, but will also often prove to be a rather smart choice.

In order to keep things simple, it is necessary you isolate a niche in the development industry that interests you. Once that is achieved, research about the bare minimum of what you need to get started. The best way to go about this is to engage someone who is competent in your preferred area. In most of the cases, the insights they've gathered will save you a lot of time and agony.

In my experience, one cannot become a developer without dedicating time and effort to considerable amount of practice. I might watch loads of tutorials, read tonnes of documentation, but until I get my hands dirty, I shall remain an amateur. Looking back, it's through multiple projects (personal or otherwise), that I was able to [drown my impostor's syndrome](https://blog.teamtreehouse.com/slaying-the-dragon-imposter-syndrome).

### It's is really hard to be a jack of all trades
![disillusioned](/assets/posts/disillusioned.jpg)

It's not uncommon wanting to be the ultimate computer wizard. The guru who knows how to expeditiously put up a grand website, make an android application, an IOS application, hack into your neighbor's network, understand how neural engines work. All that stuff that movies tell us is not only super cool but also the real mark of a genius. The truth is, however, well, something else. 

Each of the areas mentioned above are vast on their own, thus the chances that any one of us would master any two of these areas concurrently are daunting. 
With that background I cannot help but chuckle at anyone who wants to learn more than one of these fields concurrently. While I'm not one to believe in the word *"impossible"*, I have the wisdom to see *"improbable"* endeavors for what they are. 
It's far much probable to master one field after conquering another, than it is to take them on concurrently.

### The rabbit hole goes much deeeper

There are at least a dozen of reasons why what was once a burning zeal to become an exceptional developer is now just but a distant memory. The journey is certainly not for the faint of heart. Nor is it for the undisciplined. Here, only those that are fit survive to code some more.