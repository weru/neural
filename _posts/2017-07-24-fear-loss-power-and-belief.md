---
tags:
  - perspective
image: power
---

They say everyone is afraid of something. What they fail to mention is that it is different for everyone. You probably would understand why that is. It is for the simple fact that everyone is not afraid of the same thing. That said, there are things that people collectively. Even then, the degree of dread for those things vary among individuals. What are some of these things that most people dread? If you ask me, I would say, change, pain, loss and 'the unknown'. That would sound like quite a list of dreads, but a wise person will spot them as one – the three sides of the same coin. 


If I'm to be asked, again, I would say that all of these things are dread-inducing because they result in loss. If something changes in the way things are done, said or perceived, we fear that we might lose that which we possess in the moment. Privileges, comfort, company, loyalties ... I have to stop at this point for an obvious reason.  Again, this is the same subject dressed in different labels. We are afraid to lose the perceived power that we think we wield at the moment. 


We humans model our lives around the cultivation and accumulation of power. The goal, always, is to acquire the greatest possible amount of it possible. And why is that? Well, because power has the potential to safeguard itself and to an extent those that wield it. However, absent meaningful awareness of how to hold that power, it often devours us. 

Hmm, no wonder the uninitiated cry foul of it. They chant that it is destructive - and I wouldn't dare argue to the extent that it indeed is, to them. Yet in spite of their cry, not one of them can help but desire it in some form of their choosing, indifference or ignorance.


Although the assertion that all of us desire power in whatever manner that we so happen to, few, if any, can stomach the thought of losing it. That said, most of us wouldn't really know. If we could identify power for what it is, we would, but we are often not consciously aware - thus our failure. 

We have our heads buried in the sand. After all, we have conditioned ourselves to recognize power only in it overt manifestations. We perceive a politician, a CEO, a billionaire or millionaire, a judge, a soldier as powerful figures. These few classes form our cluster of kings, the custodians of power and the butlers of it – people that we naively look up to as though they are the sole bankers of the power.


It is interesting how we even fail to spot the ones that hold power, sometimes more of it than our cluster of ‘obvious kings’ in the shadows. Such include the invisible ghosts behind these ‘kings’. Although this is entirely a topic of a different time, I must not forget to draw attention to religious leaders. So while we lightly scout the helms of this side matter, I will unmask an elusive secret. Religious leaders form a class that hide the tremendous power they wield in plain sight. 

They, more than everyone else do so at very little expense, accountability and risk of volatility – the men with invisible webs cast over people’s spirits, mind and sense of morality.  Even the very ‘insinuations’ that they are really not accountable will sound strange, unbecoming, and may be even blasphemous. Men accountable to no other but The Good Lord – as if HE did not deem fit that men too, be accountable to each other. They are not gods, or are they? 

Anyway, they always profess to talk for The Good Lord, even when they don’t. I assure you most of them often do not. It would then follow that since they often fail to speak on behalf of THE GREAT I AM – the only one they apparently profess to be accountable to – they are thus not accountable to anyone in and outside our realm – only but a few. Yet they have their webs cast over the hearts of men.

Back to our main stream of thought, you might realize that power is a tricky subject. If you followed closely, power is both tangible and intangible. That said, all source of it is intangible and it would therefore follow that to hold on to it, one must have a concept of the intangible. I once heard that *power resides where people believe it does*. 

Today and the number of days before it when I have had this awareness, it is clear in my mind that at its core, power is faith – the belief and the ability to amplify it among the masses, in our communities. The implications of that awareness are liberating to say the least. It corners a host of fears that we hold, and to a large extent the need to be dependent.

It is true that everyone is innately powerful. Few, however, have the knowledge and capacity requisite to manifest their power, their light. This reality is what it is because people fail to recognize the power within, the power that is buried down deep inside. It is indeed saddening that due to our petty proclivities, we fail to appreciate the real value of subtleness. 

As I hinted before, the most powerful among us are those that wield subtle, covert, implicit power. This kind of power transcends all other forms of power for a simple truth. It does not attract attention, thus it is immune to envy, competition and malice. Secondly, this form of power is disarming because it is seemingly unthreatening.

How does this explanation then relate to ‘the powerless’ and or the fears that we all constantly feel? If we knew that the basis and real substance of power is absolute belief (faith), we would squash a large proportion of unhealthy fears – FYI, some degree of it is healthy. Instead of making other people the objects of our envy, we would look inside and realize our own greatness. We would remember the words of Marcus Aurelius as if they are our very own, and the lessons there-in. This great Roman General and Emperor, took note:

> *"All is ephemeral, - fame and the famous as well"*

More importantly, we would live our lives not always with our breaks on, but only when it is absolutely necessary (healthy fears). Our stiffled subconcious selves are cognizant of our true selves, what we are really capable of. If not so, they once did know - although I would bet on everything that they haven't forgotten. The lingering sparkle of belief in a child eyes, until the world teaches it otherwise.  There it is.
 
The lies that lay claim of our beings and colonizes us. Teaching us to see only a shadow of ourselves when we look at ourselves in the mirror. The awareness that nothing but time has really been lost will be liberating. Awaken and shun the belittling fears that creep up on you when you want to do something bold. If you truly believe, the power is yours, it has always been. Lean inward, discover the possibilities unexplored. Use that awareness to do good, spread the positive energy.

As a final side note, if one could amass power without the fame that so often accompanies it, that would probably be the best outcome possible. In my mind, fame is good only to the extent that even when you are idolized, you are simultaneously underestimated. Being underestimated is a gift.
