<header class = 'nav {% if page.dark %} darken {% endif %}' >
  <nav class = 'nav-menu'>
    <a href='{{ '/' }}' class = 'nav-brand'>{{ site.title }}</a>
    <div class = 'nav-bar'>&#9776;</div>
  </nav>
</header>
