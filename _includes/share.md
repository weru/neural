<div class = 'share-trigger'>
  <img src = '/assets/icons/share.svg' alt = 'share'>
</div>
<div class = 'share'> 
  <div class = 'share_card'>
    <a class = 'facebook' href="https://www.facebook.com/sharer/sharer.php?u={{ site.url }}{{site.baseurl}}{{ page.url }}"
    onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" >
      <img src = '/assets/icons/facebook.svg' alt = 'share'> Facebook
    </a>
    <a class = 'twitter' href="https://twitter.com/intent/tweet?text={{ page.title }}&url={{ site.url }}{{site.baseurl}}{{ page.url }}" 
    onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;">
      <img src = '/assets/icons/twitter.svg' alt = 'share'> Twitter
    </a>
  </div>
  <div class = 'share_card-bottom'>
  </div>
</div>
