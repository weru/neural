<div class = 'nav-drop'>
  <div class = 'nav-body'>
    <a href = '{{site.baseurl}}/author/'>Author</a>
    <a href="{{ '/' | relative_url}}">Blog</a>
    <a href = '{{site.baseurl}}/author/#projects'>Projects</a>
    <a href = '{{site.baseurl}}/tag/perspective/'>Perspective</a>
    <a href = '{{site.baseurl}}/tag/tech/'>Tech</a>
    <div class = 'nav-close'></div>
  </div>
</div>
