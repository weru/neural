<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <meta name="google-site-verification" content="{{ site.google-verification-code }}" />
  <meta name="msvalidate.01" content="{{ site.ms-verification-code }}" />
  <title>{% if page.title %}{{page.title}} | {% endif %}{{site.title}}</title>
  {% if page.noindex  %}
    <meta name='robots' content='noindex' />
  {% endif %}
 {% if jekyll.environment == 'production' %} 
  {% include analytics.md %}{% endif %}
 {% assign excerpt = page.excerpt | default: site.description | strip_html | normalize_whitespace | truncate: 160 | escape %}
  <meta property='og:site_name' content='{{site.title}}' />
  <meta itemprop = 'description' name='description' content='{{ excerpt }}'>
  <meta property='og:description' content='{{ excerpt }}'>
  <meta property='og:locale' content='en_US' />
  {% if page.layout == 'post' %}
    <meta property='og:type' content='article' />
    <meta property='og:title' content='{{page.title}}' />
    <meta property='og:url' content='{{site.url}}{{page.url}}' />
    <meta property='article:published_time' content='{{page.date | date_to_xmlschema}}' />
    <script type='application/ld+json'>
    {
      "@context":"http://schema.org",
      "@type":"Article",
      "mainEntityOfPage":"{{site.url}}{{page.url}}",
      "url":"{{site.url}}{{page.url}}",
      "headline":"{{page.content | strip_html | truncate: 50 }}",
      "description":"{{page.content | strip_html | truncate: 160 }}",
      "keywords":"{{ page.keywords | join: ', ' }}",
      "image":{  
          "@type":"ImageObject",
          "url":"{{site.url}}/assets/posts/{{page.image}}.jpg"
      },
      "author":{  
          "@type":"Person",
          "name":"{{site.author}}"
      },
      "datePublished":"{{ page.date | date_to_xmlschema }}",
      "dateModified":"{{ page.date | date_to_xmlschema }}",
      "publisher":{  
          "@type":"Organization",
          "name":"{{ site.title }}",
          "logo":{  
            "@type":"ImageObject",
            "url":"{{site.url}}/assets/posts/{{page.image}}.jpg"
          }
      }
    }
    </script>
  {% endif %}
  {% if page.image %}
    <meta property='og:image' content='{{site.url}}/assets/posts/{{page.image}}.jpg' />
    <meta property='og:image:width' content='720' />
    <meta property='og:image:height' content='360' />
    <meta name='twitter:site' content='@{{site.twitter}}' />
    <meta name='twitter:image' content='{{site.url}}/assets/posts/{{page.image}}.jpg' />
    <meta name='twitter:card' content='summary' />
    <meta name='twitter:creator' content='@{{site.twitter}}' />
    <meta property='og:description'  content='{{ page.excerpt | strip_html }}'/>
  {% endif %}
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/icons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/icons/favicon-16x16.png">
  <link rel="manifest" href="/assets/icons/site.webmanifest">
  <link rel="mask-icon" href="/assets/icons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <link rel='canonical' href="{{ page.url | replace:'index.html','' | absolute_url }}">
  <link href="{{ site.fonts }}" rel="stylesheet">
  <link href = '/assets/main.css' rel = 'stylesheet'>
  <style>
    .cool{
      background-image:url(/assets/posts/{{page.image}}.jpg);
    }
  </style>
</head>
