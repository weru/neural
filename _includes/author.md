{% if page.author %}
  {% assign name = page.author | capitalize %}
  {% for author in site.data.authors %}
  {% if author.name contains name %}
  <a class = 'flex-in' href = '{{ site.designer.url }}' target = '_blank' rel = 'nonopener' target = 'Linkedin profile'><img class = 'gravatar' src = '{{site.baseurl}}/assets/authors/{{author.avatar}}' alt = '{{author.name}}'></a>
  {% endif %}
  {% endfor %}
{% endif %}