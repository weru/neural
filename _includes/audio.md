<div class = 'waves'>
  <canvas id = 'waves' class = 'waves_inner'></canvas>
</div>
<script>
  {% include sine-waves.min.js %}
  {% include audio.js %}
</script>
