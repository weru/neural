<footer class = 'footer'>
  <div>{% include chimp.md %}</div>
  <div class = 'footer_inner'>
    <p>
      <span>{{ site.title }} <span class = 'year'> </span><span class = 'apart'> |</span> All rights Reserved.</span>
    </p>
    <p class = 'mute'>
      <small>Designed by</small> <a href = '{{site.designer.url}}' target = '_blank' title = 'Linkedin Profile' rel = 'nonopener'> {{ site.designer.name | capitalize }}</a>
    </p>
  </div>
</footer>
<script   src='https://code.jquery.com/jquery-3.1.0.min.js'   integrity='sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s='   crossorigin='anonymous'></script>
<script>
  {% include time.js %}
  {% include autosize.min.js %}
  {% include index.js %}
</script>
<!-- mailchimp -->
<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","{{ site.mailchimp.validate }}");</script>
