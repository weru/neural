<li class = 'third post_item'>
  <a class = 'show' href='{{ post.url | relative_url }}' title = '{{ post.title }}'
    style = 'background-image: url(/assets/posts/thumbnails/{{ post.image }}-min.jpg);'>
    <div class = 'overlay flex-in'>
      <div>
      </div>
    </div>
  </a>
  <div class = 'excerpt'>
    {% assign tag = post.tags | first %}
    <p class = 'post-time'>
      <span class = 'pale'>{% assign time = post.content| number_of_words | divided_by:180 | ceil  %}{{ time }} min read</span>
        <span class = 'apart'>|</span>
        <span> {{ tag }}</span>
    </p>
    <h3 class = 'post-link'>
      <a href='{{ post.url | relative_url }}'>{{ post.title | escape  }}</a>
    </h3>
    {% if excerpt %}<p class = 'pale'>{{ post.excerpt | truncate: 90 | strip_html }}</p>{% endif %}
  </div>
</li>