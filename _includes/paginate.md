{% if paginator.total_pages > 1 %}
  <div class="pagination"> 
  {% if paginator.previous_page %}
    <a href = '{{ paginator.previous_page_path | relative_url }}'>&laquo;</a>
    {% else %}
      <span>&laquo; </span>
    {% endif %}
    {% if page.layout == 'blog' %}
    {% for page in (1..paginator.total_pages) %}
      {% if page == paginator.page %}
        <span class="active">{{ page }}</span>
      {% elsif page == 1 %}
        <a href="/">{{ page }}</a>
      {% else %}
        <a href="/{{ forloop.index }}">{{ page }}</a>
      {% endif %}
    {% endfor %}
    {% endif %}
    {% if paginator.next_page %}
    <a href = '{{ paginator.next_page_path | relative_url }}'> &raquo;</a>
    {% else %}
      <span> &raquo;</span>
    {% endif %}
  </div>
{% endif %}

