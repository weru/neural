---
layout: default
comments: true
published: true
---
<div class = 'tablet flex-in'>
</div>
<div class = 'cool libro'>
 <div class = 'overlay'>
   <div class='post-header'>
    <div class = 'cover'>
      <h1 class='post-title' itemprop='name headline'>{{ page.title | escape }}</h1>
      {% include author.md %}
    </div>
   </div>
 </div>
</div>
<article class = 'post'>
  <div class = 'post__section'>
    <div class='post-content' itemprop='articleBody'>
    {% assign tag = page.tags | first %}
    <div class = ''>
      <div class = 'post-time'>
        <span class = 'post-time-item'>{% assign time = page.content| number_of_words | divided_by:180 | ceil  %}{{ time }} min read</span>
          <span class = 'apart post-time-item' >|</span>
          <span class = 'post-time-item'> {{ tag }}</span>
      </div>
    </div>
      <div class = 'articulo'>{{ content }}</div>
      {% include share.md %}
    </div>
    {% include comments.html %}
  </div>
  <aside class = 'post__section'>
    <h3><span class = 'pretty'>Recent Posts</span></h3>
    <ul class='flex post__aside'>
      {% assign recent = site.posts | where_exp: "item", "item != page" %}
      {% for post in  recent limit:3 %}
       {% include excerpt.md %}
      {% endfor %}
    </ul>
  </aside>
</article>
